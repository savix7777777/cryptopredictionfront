import React, { lazy, ReactElement, Suspense, useEffect } from 'react';
import { useLocation } from 'react-router';
import { useRoutes, Navigate } from 'react-router-dom';
import { createPortal } from 'react-dom';

import { Modal } from 'components/molecules';
import { ProtectedRoute } from 'components/templates';
import { NotFound } from 'components/atoms';
import { autoLogin } from 'redux/ActionsCreator';
import { useAppDispatch } from 'hooks/useAppDispatch';
import { useAppSelector } from './hooks/useAppSelector';

import PATHS from 'routes/PATHS';

const HomePage = lazy(() => import('components/pages/Home'));
const CryptoPage = lazy(() => import('components/pages/Crypto'));
const CryptoDetailPage = lazy(() => import('components/pages/CryptoDetail'));
const RoadmapPage = lazy(() => import('components/pages/Roadmap'));
const LoginPage = lazy(() => import('components/pages/Login'));
const RegisterPage = lazy(() => import('components/pages/Register'));
const ResetPasswordPage = lazy(() => import('components/pages/ResetPassword'));

import cn from 'classnames';

const App = (): ReactElement => {
  const location = useLocation();

  const locationArr = location.pathname.split('/');

  const dispatch = useAppDispatch();

  const { user } = useAppSelector((state) => state.userReducer);
  const { modal } = useAppSelector((state) => state.modalReducer);
  const { dark } = useAppSelector((state) => state.themeReducer);

  const determineBackground = (): string => {
    switch (locationArr[locationArr.length - 1]) {
      case PATHS.HOME:
        return cn('background background-main', !dark ? 'light' : 'dark');
      case PATHS.LOGIN:
        return 'background background-login';
      case PATHS.RESET_PASSWORD:
        return 'background background-login';
      case PATHS.REGISTER:
        return 'background background-register';
      case PATHS.CRYPTO:
        return cn('background', 'background-scroll', !dark ? 'light' : 'dark');
      default:
        return cn('background', !dark ? 'light' : 'dark');
    }
  };

  useEffect(() => void dispatch(autoLogin() as any), []); // eslint-disable-line @typescript-eslint/no-explicit-any

  const page = useRoutes([
    {
      path: PATHS.ROOT,
      children: [
        { index: true, element: <Navigate to={`${PATHS.MAIN}/${PATHS.HOME}`} /> },
        {
          path: PATHS.MAIN,
          element: <ProtectedRoute isAllowed={true} />,
          children: [
            { index: true, element: <Navigate to={`/${PATHS.MAIN}/${PATHS.HOME}`} /> },
            {
              path: PATHS.HOME,
              children: [
                {
                  index: true,
                  element: (
                    <Suspense fallback={<div className={'background background-main'} />}>
                      <HomePage />
                    </Suspense>
                  ),
                },
              ],
            },
            {
              path: PATHS.CRYPTO,
              children: [
                {
                  index: true,
                  element: (
                    <Suspense fallback={<></>}>
                      <CryptoPage />
                    </Suspense>
                  ),
                },
                {
                  path: ':cryptocurrency',
                  element: (
                    <Suspense fallback={<div className={'background'} />}>
                      <CryptoDetailPage />
                    </Suspense>
                  ),
                },
              ],
            },
            {
              path: PATHS.ROADMAP,
              children: [
                {
                  index: true,
                  element: (
                    <Suspense fallback={<div className={'background'} />}>
                      <RoadmapPage />
                    </Suspense>
                  ),
                },
              ],
            },
          ],
        },
        {
          path: PATHS.LOGIN,
          element: user.id && <Navigate to={`/${PATHS.MAIN}/${PATHS.HOME}`} />,
          children: [
            {
              index: true,
              element: (
                <Suspense fallback={<div className={'background background-login'} />}>
                  <LoginPage />
                </Suspense>
              ),
            },
          ],
        },
        {
          path: PATHS.REGISTER,
          element: user.id && <Navigate to={`/${PATHS.MAIN}/${PATHS.HOME}`} />,
          children: [
            {
              index: true,
              element: (
                <Suspense fallback={<div className={'background background-register'} />}>
                  <RegisterPage />
                </Suspense>
              ),
            },
          ],
        },
        {
          path: PATHS.RESET_PASSWORD,
          element: user.id && <Navigate to={`/${PATHS.MAIN}/${PATHS.HOME}`} />,
          children: [
            {
              index: true,
              element: (
                <Suspense fallback={<div className={'background background-login'} />}>
                  <ResetPasswordPage />
                </Suspense>
              ),
            },
          ],
        },
        {
          path: '*',
          element: <NotFound />,
        },
      ],
    },
  ]);

  return (
    <>
      <div className={determineBackground()}>
        <div className={cn('backdrop-filter', !dark ? 'light' : 'dark')} />
        {page}
      </div>

      {modal && createPortal(<Modal />, document.querySelector('#modal') || document.body)}
    </>
  );
};

export default App;
