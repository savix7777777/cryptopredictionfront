import React, { ComponentPropsWithoutRef, forwardRef } from 'react';
import { useAppSelector } from 'hooks/useAppSelector';

import cn from 'classnames';

import './Button.scss';

export type ButtonProps = ComponentPropsWithoutRef<'button'> & {
  primary?: boolean;
  outline?: boolean;
};

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  ({ children, disabled, className, type, primary, outline, ...otherProps }: ButtonProps, ref) => {
    const { dark } = useAppSelector((state) => state.themeReducer);

    return (
      <button
        {...otherProps}
        ref={ref}
        type={type || 'button'}
        disabled={disabled}
        className={cn(
          'button',
          className,
          primary && 'primary',
          outline && 'outline',
          !dark ? 'light' : 'dark',
          {
            disabled,
          },
        )}
      >
        {children}
      </button>
    );
  },
);

export default Button;
