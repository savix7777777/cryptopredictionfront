import React, {
  ChangeEvent,
  ComponentPropsWithoutRef,
  Dispatch,
  forwardRef,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react';

import cn from 'classnames';

import './ControlledInput.scss';

type InputProps = ComponentPropsWithoutRef<'input'> & {
  update: (val: string) => void;
  clearInput?: boolean;
  setClear?: Dispatch<SetStateAction<boolean>>;
  updateInputValue?: string;
  setUpdateInputValue?: Dispatch<SetStateAction<string>>;
  englishOnly?: boolean;
};

const ControlledInput = forwardRef<HTMLInputElement, InputProps>(
  (
    {
      type,
      className,
      update,
      clearInput,
      setClear,
      updateInputValue,
      setUpdateInputValue,
      englishOnly = false,
      ...props
    }: InputProps,
    ref,
  ) => {
    const [value, setState] = useState<string>('');

    useEffect(() => {
      if (clearInput && setClear) {
        setState('');
        setClear(false);
      }
    }, [clearInput]);

    useEffect(() => {
      if (updateInputValue && setUpdateInputValue) {
        setState(value + updateInputValue);
        update(value + updateInputValue);
        setUpdateInputValue('');
      }
    }, [updateInputValue]);

    const handleChange = useCallback(
      (e: ChangeEvent<HTMLInputElement>) => {
        let newValue = e.target.value;

        if (englishOnly) {
          newValue = newValue.replace(/[^a-zA-Z0-9\s!@#$§%^&*()_+\-=[\]{};':"\\|,.<>/?]+/g, '');
        }

        setState(newValue);
        update(newValue);
      },
      [englishOnly, update],
    );

    return (
      <input
        {...props}
        ref={ref}
        type={type || 'text'}
        id={'controlled-input'}
        value={value}
        onChange={handleChange}
        className={cn('input', className)}
      />
    );
  },
);

export default ControlledInput;
