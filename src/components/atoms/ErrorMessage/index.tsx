import React, { ComponentPropsWithoutRef, ReactElement } from 'react';
import cn from 'classnames';

import './ErrorMessage.scss';

type DefaultErrorMessageProps = {
  errorHint: string;
  errorText: string;
};

type ErrorMessagePropsType = ComponentPropsWithoutRef<'div'> & DefaultErrorMessageProps;

const ErrorMessage = ({ className, errorHint, errorText }: ErrorMessagePropsType): ReactElement => (
  <div className={cn('error', className)}>
    <p className={'error-text'}>
      {errorHint}
      {errorText ? ': ' + errorText : ''}
    </p>
  </div>
);

export default ErrorMessage;
