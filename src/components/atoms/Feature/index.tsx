import React, { ReactElement } from 'react';
import cn from 'classnames';

import { useAppSelector } from 'hooks/useAppSelector';

import './Feature.scss';

type FeaturePropsType = {
  color: string;
  icon: ReactElement;
  heading: string;
  text: string;
};

const Feature = ({ color, icon, heading, text }: FeaturePropsType): ReactElement => {
  const { dark } = useAppSelector((state) => state.themeReducer);

  return (
    <div className={'feature-box'}>
      <div className={'feature-icon-box'} style={{ backgroundColor: color }}>
        {icon}
      </div>
      <div className={'feature-content'}>
        <h3 className={cn('feature-heading', !dark ? 'light' : 'dark')}>{heading}</h3>
        <p className={cn('feature-text', !dark ? 'light' : 'dark')}>{text}</p>
      </div>
    </div>
  );
};

export default Feature;
