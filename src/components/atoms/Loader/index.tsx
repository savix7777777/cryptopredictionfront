import React, { ComponentPropsWithoutRef, ReactElement } from 'react';
import cn from 'classnames';

import './Loader.scss';

type DefaultLoaderProps = {
  width: number;
  height: number;
  margin?: boolean;
};

type LoaderPropsType = ComponentPropsWithoutRef<'div'> & DefaultLoaderProps;

const Loader = ({ className, width, height, margin = true }: LoaderPropsType): ReactElement => (
  <div className="loader-box" style={{ margin: margin ? '150px 0' : '0' }}>
    <div
      style={{ width: `${width}px`, height: `${height}px` }}
      className={cn('loader', className)}
    />
  </div>
);

export default Loader;
