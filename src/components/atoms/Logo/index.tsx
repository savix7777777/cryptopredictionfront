import React, { ComponentPropsWithoutRef, ReactElement } from 'react';
import cn from 'classnames';
import { NavLink } from 'react-router-dom';

import PATHS from 'routes/PATHS';
import LogoSizeEnum from './types/LogoSizeEnum';

import './Logo.scss';

import foxIcon from 'assets/fox-icon.svg';

type DefaultLogoProps = {
  logo: string;
  text?: string;
  size?: LogoSizeEnum;
  icon?: boolean;
};

type LogoPropsType = ComponentPropsWithoutRef<'div'> & DefaultLogoProps;

const Logo = ({ className, logo, text, size, icon }: LogoPropsType): ReactElement => (
  <NavLink to={PATHS.ROOT}>
    <div className="logo-box">
      {icon && <img className={'logo__foxIcon'} src={foxIcon} alt={''} />}
      <img alt={'logo'} src={logo} style={{ height: `${size || LogoSizeEnum.LARGE}px` }} />
      {text && <h1 className={cn('logo-text', className)}>{text}</h1>}
    </div>
  </NavLink>
);

export default Logo;
