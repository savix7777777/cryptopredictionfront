enum LogoSizeEnum {
  SMALL = 16,
  MEDIUM = 24,
  LARGE = 32,
}

export default LogoSizeEnum;
