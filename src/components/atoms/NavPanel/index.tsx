import React, { ComponentPropsWithoutRef, ReactElement } from 'react';
import cn from 'classnames';
import { NavLink } from 'react-router-dom';

import capitalize from 'utils/capitalize';
import { NavTabType } from './types/NavTabType';
import { useAppSelector } from 'hooks/useAppSelector';

import './NavPanel.scss';
import PATHS from '../../../routes/PATHS';
import useWindowWidth from '../../../hooks/useWindowWidth';

type DefaultNavPanelProps = {
  tabs: Array<NavTabType>;
};

type NavPanelPropsType = ComponentPropsWithoutRef<'div'> & DefaultNavPanelProps;

const NavPanel = ({ className, tabs, style, onClick }: NavPanelPropsType): ReactElement => {
  const { dark } = useAppSelector((state) => state.themeReducer);

  const windowWidth = useWindowWidth();

  return (
    <div style={style || {}} className={cn('nav-panel', className)}>
      {tabs.map((item, index) => (
        <div
          onClick={onClick}
          className={cn('nav-panel-item-box', !dark ? 'light' : 'dark')}
          key={index}
        >
          <NavLink
            className={({ isActive }) =>
              cn(
                'nav-panel-item',
                isActive ? 'active' : '',
                !dark
                  ? location.pathname.endsWith(PATHS.HOME) &&
                    windowWidth <= 992 &&
                    windowWidth > 768
                    ? 'dark'
                    : 'light'
                  : 'dark',
              )
            }
            to={item.link}
          >
            {capitalize(item.title)}
          </NavLink>
        </div>
      ))}
    </div>
  );
};

export default NavPanel;
