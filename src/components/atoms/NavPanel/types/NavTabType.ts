export type NavTabType = {
  title: string;
  link: string;
};
