import React, { ReactElement, useEffect } from 'react';
import cn from 'classnames';
import { useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';

import { Button, Logo } from 'components/atoms/index';

import LogoSizeEnum from 'components/atoms/Logo/types/LogoSizeEnum';
import PATHS from 'routes/PATHS';

import logo from 'assets/logo-light.svg';

import './NotFound.scss';

const NotFound = (): ReactElement => {
  const { t } = useTranslation('NotFound');

  const navigate = useNavigate();

  useEffect(() => {
    document.querySelector('.background')?.classList.add('background-not-found');
  }, []);

  return (
    <section className={cn('section not-found')}>
      <div className={'not-found-header'}>
        <Logo
          onClick={() => navigate(`${PATHS.MAIN}/${PATHS.HOME}`)}
          logo={logo}
          size={LogoSizeEnum.MEDIUM}
        />
      </div>
      <div className={'not-found-box'}>
        <h2>404</h2>
        <h3>{t('title')}</h3>
        <hr />
        <p>
          {t('contentDescription')} <span>{t('contentAbandoned')}</span>. {t('contentNoFuture')}
          <br />
          <br />
          {t('contentAbyssDesire')} <span>{t('contentBrightReality')}</span>{' '}
          {t('contentHomeOrPrevious')}
        </p>
        <div className={'not-found-btn-box'}>
          <Button onClick={() => navigate(-1)} className={'not-found-btn'}>
            <span>{t('btnBack')}</span>
          </Button>
          <Button
            onClick={() => navigate(`${PATHS.MAIN}/${PATHS.HOME}`)}
            className={'not-found-btn not-found-btn--primary'}
          >
            <span>{t('btnHome')}</span>
          </Button>
        </div>
      </div>
    </section>
  );
};

export default NotFound;
