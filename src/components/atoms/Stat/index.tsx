import React, { ComponentPropsWithoutRef, ReactElement } from 'react';
import cn from 'classnames';
import { useTranslation } from 'react-i18next';

import { useAppSelector } from 'hooks/useAppSelector';

import './Stat.scss';

type DefaultStatProps = {
  value: string;
  text: string;
};

type StatPropsType = ComponentPropsWithoutRef<'div'> & DefaultStatProps;

const Stat = ({ className, value, text }: StatPropsType): ReactElement => {
  const { t } = useTranslation('Stats');

  const { dark } = useAppSelector((state) => state.themeReducer);

  return (
    <div className={cn('stat', className)}>
      <p className={cn('stat-text', !dark ? 'light' : 'dark')}>{t(text)}</p>
      <p className={cn('stat-value', !dark ? 'light' : 'dark')}>{value}</p>
    </div>
  );
};

export default Stat;
