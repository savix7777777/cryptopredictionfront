import React, { ReactElement } from 'react';
import cn from 'classnames';

import './Title.scss';

type TitleProps = {
  text: string;
  className?: string;
  subText?: string;
};

const Title = ({ className, text, subText }: TitleProps): ReactElement => (
  <div className={cn('title', className)}>
    <h1>{text} </h1>
    {subText && <p>{subText}</p>}
  </div>
);

export default Title;
