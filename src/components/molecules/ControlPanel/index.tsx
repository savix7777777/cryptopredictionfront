import React, { ReactElement, useCallback } from 'react';
import cn from 'classnames';
import Select from 'react-select';
import DayNightToggle from 'react-day-and-night-toggle';
import i18n from 'i18next';
import { useLocation, useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import useWindowWidth from 'hooks/useWindowWidth';
import { Button, Loader } from 'components/atoms';
import { useAppSelector } from 'hooks/useAppSelector';
import { useAppDispatch } from 'hooks/useAppDispatch';
import { userSlice } from 'redux/reducers/UserSlice';
import { themeSlice } from 'redux/reducers/ThemeSlice';
import { LOGOUT } from 'queries';

import languages, { LanguageType } from './languages';
import PATHS from 'routes/PATHS';

import './ControlPanel.scss';

const ControlPanel = (): ReactElement => {
  const { t } = useTranslation('ControlPanel');
  const location = useLocation();

  const navigate = useNavigate();

  const dispatch = useAppDispatch();

  const { user, isLoading } = useAppSelector((state) => state.userReducer);
  const { dark } = useAppSelector((state) => state.themeReducer);

  const windowWidth = useWindowWidth();

  const { clearUser } = userSlice.actions;
  const { updateTheme } = themeSlice.actions;

  const handleLogout = useCallback(() => {
    void LOGOUT().then((response) => {
      if (response.success) {
        dispatch(clearUser());
      }
    });
  }, [dispatch]);

  const handleChangeLanguage = useCallback((item: LanguageType | null) => {
    if (item) {
      void i18n.changeLanguage(item.i18);
      localStorage.setItem('language', item.i18);
    }
  }, []);

  const determinateDefaultLanguage = useCallback(() => {
    const language = localStorage.getItem('language');
    if (language)
      return (
        languages.find((item) => item.i18 === language) || { value: 'eng', label: 'ENG', i18: 'en' }
      );
    return { value: 'eng', label: 'ENG', i18: 'en' };
  }, []);

  return (
    <div className={cn('control-panel', !dark ? 'control-panel-light' : '')}>
      <div className={'control-panel__controllers'}>
        <DayNightToggle
          className={cn('control-panel__controllers-theme', !dark ? 'light' : 'dark')}
          onChange={() => dispatch(updateTheme(!dark))}
          checked={dark}
          startInactive={false}
          size={windowWidth <= 768 ? 28 : 20}
        />
        <Select
          className={'control-panel__controllers-lang'}
          styles={{
            option: (provided, state) => ({
              ...provided,
              cursor: 'pointer',
              color: state.isSelected && !dark ? '#fff !important' : provided.color,
              background: state.isSelected
                ? !dark
                  ? '#5EBBFE !important'
                  : '#7F8AF2 !important'
                : '',
            }),
            control: (provided) => ({
              ...provided,
              minHeight: windowWidth > 768 ? '32px' : '40px',
              fontSize: '12px',
              borderRadius: '4px',
              border: 'none',
              cursor: 'pointer',
              boxShadow: '0 0 0 1px transparent',
            }),
            input: (provided) => ({
              ...provided,
              margin: '0 2px',
              padding: '0',
            }),
            indicatorsContainer: (provided) => ({
              ...provided,
              height: windowWidth > 768 ? '32px' : '40px',
            }),
            menu: (provided) => ({
              ...provided,
              cursor: 'pointer',
              borderRadius: '4px',
              boxShadow: 'none',
              fontSize: '12px',
            }),
          }}
          classNames={{
            control: () =>
              cn(
                'control-panel__controllers-lang__control',
                !dark
                  ? location.pathname.endsWith(PATHS.HOME) && windowWidth > 768
                    ? 'white'
                    : 'light'
                  : 'dark',
              ),
            singleValue: () =>
              cn('control-panel__controllers-lang__singleValue', !dark ? 'light' : 'dark'),
            input: () => cn('control-panel__controllers-lang__input', !dark ? 'light' : 'dark'),
            menu: () => cn('control-panel__controllers-lang__menu', !dark ? 'light' : 'dark'),
            option: () => cn('control-panel__controllers-lang__option', !dark ? 'light' : 'dark'),
            dropdownIndicator: () =>
              cn('control-panel__controllers-lang__indicator', !dark ? 'light' : 'dark'),
          }}
          onChange={(item) => handleChangeLanguage(item)}
          menuPlacement={'auto'}
          noOptionsMessage={() => 'Nothing'}
          options={languages}
          defaultValue={determinateDefaultLanguage()}
        />
      </div>
      <div className={'control-panel__auth-box'}>
        {isLoading ? (
          <Loader width={25} height={25} margin={false} />
        ) : user.id ? (
          <Button
            onClick={handleLogout}
            className={cn(
              'control-panel__logout',
              !dark && location.pathname.endsWith(PATHS.HOME) && windowWidth > 768 ? 'white' : '',
            )}
          >
            <FontAwesomeIcon icon={icon({ name: 'right-from-bracket' })} />
            {windowWidth <= 768 && <p className={'control-panel__logout-text'}>{t('btnLogout')}</p>}
          </Button>
        ) : (
          <>
            <Button
              className={cn(
                'control-panel__login',
                !dark && location.pathname.endsWith(PATHS.HOME) && windowWidth > 768 ? 'white' : '',
              )}
              onClick={() => navigate(`/${PATHS.LOGIN}`)}
            >
              {t('btnLogin')}
            </Button>
            <Button
              className={'control-panel__register'}
              onClick={() => navigate(`/${PATHS.REGISTER}`)}
              primary
            >
              {t('btnSignup')}
            </Button>
          </>
        )}
      </div>
    </div>
  );
};

export default ControlPanel;
