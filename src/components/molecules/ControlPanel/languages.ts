export type LanguageType = {
  value: string;
  label: string;
  i18: string;
};

const languages: Array<LanguageType> = [
  { value: 'eng', label: 'ENG', i18: 'en' },
  { value: 'deu', label: 'DEU', i18: 'de' },
  { value: 'ukr', label: 'UKR', i18: 'uk' },
];

export default languages;
