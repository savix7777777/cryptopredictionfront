import React, { ReactElement, useMemo } from 'react';
import cn from 'classnames';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from 'react-i18next';

import capitalize from 'utils/capitalize';
import { useAppSelector } from 'hooks/useAppSelector';

import { ICryptoDetailData } from 'queries/globalData/GET_CRYPTOS_LIST_DATA';

import './CryptoTitle.scss';

const CryptoTitle = ({ data, name }: { data: ICryptoDetailData; name: string }): ReactElement => {
  const { t } = useTranslation('CryptoTitle');

  const { dark } = useAppSelector((state) => state.themeReducer);

  const calculatePercents = useMemo(
    () => ((data.data1D.price - +data.currentPrice) * 100) / +data.currentPrice,
    [data.data1D.price, data.currentPrice],
  );

  return (
    <div className={cn('crypto-title', !dark ? 'light' : 'dark')}>
      <div className={'crypto-title__name-box'}>
        <img src={require(`cryptocurrency-icons/128/color/${data.icon}.png`)} alt={data.icon} />
        <p className={'crypto-title__name'}>
          {name.length === 3 ? name.toUpperCase() : capitalize(name)}
        </p>
        <p className={'crypto-title__symbol'}>{data.symbol.toUpperCase()}</p>
      </div>
      <div className={'crypto-title__price-box'}>
        <p className={'crypto-title__price'}>${data.currentPrice}</p>
        <p
          className={cn(
            'crypto-title__percent',
            calculatePercents > 0
              ? 'crypto-title__percent-positive'
              : 'crypto-title__percent-negative',
            !dark ? 'light' : 'dark',
          )}
        >
          {calculatePercents > 0 ? (
            <FontAwesomeIcon
              className={'crypto-title__arrow-sort'}
              icon={icon({ name: 'sort-up' })}
            />
          ) : (
            <FontAwesomeIcon
              className={'crypto-title__arrow-sort-revert'}
              icon={icon({ name: 'sort-down' })}
            />
          )}
          {calculatePercents.toFixed(2)}% ({t('eveningPrediction')})
        </p>
      </div>
    </div>
  );
};

export default CryptoTitle;
