import React, { ReactElement } from 'react';
import Carousel from 'better-react-carousel';
import cn from 'classnames';

import { useAppSelector } from 'hooks/useAppSelector';
import useWindowWidth from 'hooks/useWindowWidth';

import './CustomCarousel.scss';

const CustomCarousel = ({ slides }: { slides: string[] }): ReactElement => {
  const { dark } = useAppSelector((state) => state.themeReducer);

  const windowWidth = useWindowWidth();

  return (
    <Carousel
      width="100%"
      cols={1}
      rows={1}
      gap={windowWidth <= 768 ? 30 : 20}
      loop
      hideArrow
      autoplay={5000}
      containerStyle={{ marginBottom: '8px' }}
    >
      {slides.map((item, index) => (
        <Carousel.Item key={index}>
          <div className={cn('carousel-item-box', !dark ? 'light' : 'dark')}>{item}</div>
        </Carousel.Item>
      ))}
    </Carousel>
  );
};
export default CustomCarousel;
