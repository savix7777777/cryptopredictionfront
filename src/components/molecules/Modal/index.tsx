import React, { ReactElement, useEffect } from 'react';
import cn from 'classnames';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';

import useWindowWidth from 'hooks/useWindowWidth';
import { Button, Feature } from 'components/atoms';
import { useAppDispatch } from 'hooks/useAppDispatch';
import { modalSlice } from 'redux/reducers/ModalSlice';
import { useAppSelector } from 'hooks/useAppSelector';

import PATHS from 'routes/PATHS';

import './Modal.scss';

const Modal = (): ReactElement => {
  const { t } = useTranslation('Modal');

  const { dark } = useAppSelector((state) => state.themeReducer);

  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const windowWidth = useWindowWidth();

  const { updateModal } = modalSlice.actions;

  useEffect(() => {
    const listener = (event: { code: string; preventDefault: () => void }) => {
      if (event.code === 'Escape') {
        event.preventDefault();
        dispatch(updateModal(false));
      }
    };
    document.addEventListener('keydown', listener);
    return () => {
      document.removeEventListener('keydown', listener);
    };
  });

  return (
    <div className={'modal-overlay'} onClick={() => dispatch(updateModal(false))}>
      <div
        className={cn('modal-box', !dark ? 'light' : 'dark')}
        onClick={(event) => event.stopPropagation()}
      >
        <FontAwesomeIcon
          onClick={() => dispatch(updateModal(false))}
          className={cn('close-btn', !dark ? 'light' : 'dark')}
          icon={icon({ name: 'close' })}
        />
        <div className={'modal-header'}>
          <h3 className={'modal-subheading'}>{t('modalSubTitle')}</h3>
          <h2 className={cn('modal-heading', !dark ? 'light' : 'dark')}>
            {t(windowWidth < 576 ? 'modalTitle' + 'Short' : 'modalTitle')}
          </h2>
        </div>
        <div className={'modal-grid'}>
          <Feature
            color={'palevioletred'}
            heading={t('featureTrackTitle')}
            text={t('featureTrackText')}
            icon={<FontAwesomeIcon className={'feature-icon'} icon={icon({ name: 'star' })} />}
          />
          <Feature
            color={'#72A0FF'}
            heading={t('featureVoteTitle')}
            text={t('featureVoteText')}
            icon={<FontAwesomeIcon className={'feature-icon'} icon={icon({ name: 'thumbs-up' })} />}
          />
          <Feature
            color={'#76ce9f'}
            heading={t('featureFreeTitle')}
            text={t('featureFreeText')}
            icon={
              <FontAwesomeIcon className={'feature-icon'} icon={icon({ name: 'sack-dollar' })} />
            }
          />
          <Feature
            color={'#F9AA4E'}
            heading={t('featureInformedTitle')}
            text={t('featureInformedText')}
            icon={<FontAwesomeIcon className={'feature-icon'} icon={icon({ name: 'bell' })} />}
          />
        </div>
        <div className={'modal-btn-group'}>
          <Button
            onClick={() => {
              navigate(`/${PATHS.REGISTER}`);
              dispatch(updateModal(false));
            }}
            className={cn('modal-btn', 'modal-btn-register')}
            primary={true}
          >
            {t(windowWidth < 576 ? 'btnRegister' + 'Short' : 'btnRegister')}
          </Button>
          <Button
            onClick={() => {
              navigate(`/${PATHS.LOGIN}`);
              dispatch(updateModal(false));
            }}
            className={'modal-btn'}
          >
            {t('btnLogin')}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
