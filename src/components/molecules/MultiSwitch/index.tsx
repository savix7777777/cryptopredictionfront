import React, { useState, useEffect, useRef } from 'react';
import './MultiSwitch.scss';

interface CustomMultiSwitchProps {
  texts: string[];
  selectedSwitch?: number;
  bgColor?: string;
  borderColor?: string;
  borderWidth?: string;
  fontColor?: string;
  selectedFontColor?: string;
  selectedSwitchColor?: string;
  fontSize?: string;
  fontWeight?: string;
  onToggleCallback?: (id: number) => void;
  multiSwitchWidth?: string;
  height?: string;
}

const CustomMultiSwitch: React.FC<CustomMultiSwitchProps> = ({
  texts = ['Text 1', 'Text 2'],
  selectedSwitch = 0,
  bgColor = 'white',
  borderColor = 'black',
  borderWidth = '0.1rem',
  fontColor = 'black',
  selectedFontColor = 'yellow',
  selectedSwitchColor = 'teal',
  multiSwitchWidth = '100%',
  height = '30px',
  fontSize = '12px',
  fontWeight = 'bold',
  onToggleCallback,
}) => {
  const [selected, setSelected] = useState(selectedSwitch);
  const [calculatedWidth, setCalculatedWidth] = useState<number>(0);

  const switchContainerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const switchContainer = switchContainerRef.current;
    if (switchContainer) {
      const resizeObserver = new ResizeObserver((entries) => {
        for (const entry of entries) {
          const newWidth = entry.contentRect.width;
          setCalculatedWidth(newWidth);
        }
      });

      resizeObserver.observe(switchContainer);

      return () => {
        if (resizeObserver && switchContainer) {
          resizeObserver.unobserve(switchContainer);
        }
      };
    }
  }, [texts.length, switchContainerRef]);

  const onToggle = (index: number) => {
    setSelected(index);
    if (onToggleCallback) {
      onToggleCallback(index);
    }
  };

  const switchStyles = {
    width: multiSwitchWidth,
    backgroundColor: bgColor,
    borderWidth: borderWidth,
    borderColor: borderColor,
    height: height,
  };

  const wrapperStyles = {
    width: multiSwitchWidth,
  };

  const labelWidth = calculatedWidth / texts.length;

  const handleStyles = {
    width: `${labelWidth}px`,
    left: `${selected * labelWidth + 2}px`,
    height: typeof height === 'string' ? `${parseInt(height, 10) - 8}px` : height,
    backgroundColor: selectedSwitchColor,
  };

  return (
    <div className="multi-switch-wrapper" style={wrapperStyles}>
      <div className="multi-switch-container" style={switchStyles} ref={switchContainerRef}>
        {texts.map((text, index) => {
          const labelStyles = {
            width: labelWidth,
            color: index === selected ? selectedFontColor : fontColor,
            lineHeight: height,
            fontSize: fontSize,
            fontWeight: fontWeight,
          };
          const classSwitchContent = `multi-switch-content ${
            index === selected ? 'multi-switch-handle-color' : ''
          }`;

          return (
            <label
              key={index}
              id={`${index}`}
              className={classSwitchContent}
              style={labelStyles}
              onClick={() => onToggle(index)}
            >
              {text}
            </label>
          );
        })}
        <span className="multi-switch-handle multi-switch-handle-move" style={handleStyles} />
      </div>
    </div>
  );
};

export default CustomMultiSwitch;
