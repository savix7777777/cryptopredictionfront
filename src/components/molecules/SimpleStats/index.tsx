import React, { ReactElement } from 'react';
import cn from 'classnames';
import { useLocation } from 'react-router';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import useWindowWidth from 'hooks/useWindowWidth';
import { ErrorMessage, Loader } from 'components/atoms';
import { GET_AVAILABLE_CRYPTOS, GET_ALL_COMMENTS_AMOUNT } from 'queries';
import { useAppSelector } from 'hooks/useAppSelector';

import PATHS from 'routes/PATHS';

import './SimpleStats.scss';

const SimpleStats = (): ReactElement => {
  const { t } = useTranslation('Stats');
  const location = useLocation();

  const windowWidth = useWindowWidth();

  const { dark } = useAppSelector((state) => state.themeReducer);

  const getStats = () =>
    Promise.all([GET_AVAILABLE_CRYPTOS(true), GET_ALL_COMMENTS_AMOUNT()]).then((res) =>
      res.map((item) => ('data' in item ? item.data : null)),
    );

  const { data, isError, error } = useQuery({
    queryKey: ['simpleStats'],
    queryFn: () => getStats(),
  });

  const calculateWorkingTime = () => {
    const currentDate = new Date();
    const releaseDate = new Date('2023-09-01');

    const differenceInTime = currentDate.getTime() - releaseDate.getTime();

    return Math.ceil(differenceInTime / (1000 * 3600 * 24));
  };

  return (
    <>
      {data ? (
        <div
          className={cn(
            'simple-stats',
            !dark ? (location.pathname.endsWith(PATHS.HOME) ? 'dark' : 'light') : 'dark',
          )}
        >
          {data.map(
            (value, index) =>
              value && (
                <p className={'simple-stats__item'} key={index}>
                  {t(
                    windowWidth < 490 || (windowWidth < 992 && windowWidth > 768)
                      ? Object.keys(value)[0] + 'Short'
                      : Object.keys(value)[0],
                  ) + ':'}
                  <span className={'simple-stats__value'}>{String(Object.values(value)[0])}</span>
                </p>
              ),
          )}
          <p className={'simple-stats__item'}>
            {t(
              windowWidth < 380 || (windowWidth < 848 && windowWidth > 768)
                ? 'workingDaysShort'
                : 'workingDays',
            )}{' '}
            : <span className={'simple-stats__value'}>{calculateWorkingTime()}</span>
          </p>
        </div>
      ) : (
        <Loader width={25} height={25} margin={false} />
      )}
      {isError && (
        <ErrorMessage
          errorHint={t('statsError')}
          errorText={error instanceof Error ? error.message : ''}
        />
      )}
    </>
  );
};

export default SimpleStats;
