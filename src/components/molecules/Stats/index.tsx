import React, { ReactElement } from 'react';
import cn from 'classnames';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { ErrorMessage, Loader, Stat } from 'components/atoms';
import {
  GET_MEAN_ACCURACY,
  GET_AVAILABLE_CRYPTOS,
  GET_ALL_COMMENTS_AMOUNT,
  GET_ALL_USERS_AMOUNT,
} from 'queries';

import { useAppSelector } from 'hooks/useAppSelector';

import './Stats.scss';

const Stats = (): ReactElement => {
  const { t } = useTranslation('Stats');

  const { dark } = useAppSelector((state) => state.themeReducer);

  const getAllStats = () =>
    Promise.all([
      GET_MEAN_ACCURACY(),
      GET_AVAILABLE_CRYPTOS(true),
      GET_ALL_COMMENTS_AMOUNT(),
      GET_ALL_USERS_AMOUNT(),
    ]).then((res) => res.map((item) => ('data' in item ? item.data : null)));

  const { data, isLoading, isError, error } = useQuery({
    queryKey: ['stats'],
    queryFn: () => getAllStats(),
  });

  return (
    <div className={cn('stats', !dark ? 'light' : 'dark')}>
      <h3 className={cn('stats-title', !dark ? 'light' : 'dark')}>{t('title')} 🔥</h3>
      <p className={cn('stats-title__text', !dark ? 'light' : 'dark')}>{t('description')}</p>
      <div className={cn('stats-separator', !dark ? 'light' : 'dark')} />
      <h3 className={cn('stats-title', !dark ? 'light' : 'dark')}>Coin Prophet</h3>
      <div className="stats-content">
        {data &&
          data.map(
            (value, index) =>
              value && (
                <Stat
                  key={index}
                  text={Object.keys(value)[0]}
                  value={String(Object.values(value)[0])}
                />
              ),
          )}
      </div>
      {isLoading && <Loader width={50} height={50} margin={false} />}
      {isError && (
        <ErrorMessage
          errorHint={t('statsError')}
          errorText={error instanceof Error ? error.message : ''}
        />
      )}
    </div>
  );
};

export default Stats;
