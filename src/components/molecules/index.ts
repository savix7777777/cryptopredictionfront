export { default as Carousel } from './CustomCarousel';
export { default as Stats } from './Stats';
export { default as SimpleStats } from './SimpleStats';
export { default as ControlPanel } from './ControlPanel';
export { default as CryptoTitle } from './CryptoTitle';
export { default as Modal } from './Modal';
