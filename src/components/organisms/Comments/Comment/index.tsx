import React, {
  ChangeEvent,
  Dispatch,
  ReactElement,
  SetStateAction,
  useEffect,
  useState,
} from 'react';
import Filter from 'bad-words';
import cn from 'classnames';
import BadWordsFilter from 'bad-words';
import { useTranslation } from 'react-i18next';
import { useQueryClient } from '@tanstack/react-query';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Button } from 'components/atoms';
import {
  DELETE_COMMENT,
  UPDATE_COMMENT,
  UPDATE_COMMENT_DISLIKES,
  UPDATE_COMMENT_LIKES,
  UPDATE_COMMENT_SENTIMENT,
  UPDATE_DISLIKES,
  UPDATE_LIKES,
} from 'queries';
import { modalSlice } from 'redux/reducers/ModalSlice';
import { userSlice } from 'redux/reducers/UserSlice';
import { useAppDispatch } from 'hooks/useAppDispatch';
import { useAppSelector } from 'hooks/useAppSelector';

import { CommentType } from 'queries/cryptoDetail/POST_COMMENT';
import { UserType } from 'queries/user/REGISTER';
import { ReplyStateType } from '../index';

import './Comment.scss';

type CommentPropsType = {
  data: CommentType;
  name: string;
  setReplyState: Dispatch<SetStateAction<ReplyStateType | null>>;
  setQueryCommentResult: Dispatch<SetStateAction<boolean | null>>;
  setLoadingState: Dispatch<SetStateAction<boolean>>;
  user: UserType;
  loadingState: boolean;
  queryCommentResult: boolean | null;
};

const nameColors = ['#72A0FF', '#E697FF', '#F9AA4E'];

const randomizeColor = () => {
  const randomIndex = Math.floor(Math.random() * nameColors.length);
  return nameColors[randomIndex];
};

const Comment = ({
  data,
  name,
  setReplyState,
  setQueryCommentResult,
  setLoadingState,
  user,
  loadingState,
  queryCommentResult,
}: CommentPropsType): ReactElement => {
  const queryClient = useQueryClient();
  const dispatch = useAppDispatch();

  const { t } = useTranslation('Comment');

  const { dark } = useAppSelector((state) => state.themeReducer);

  const [color, setColor] = useState('');
  const [optionsState, setOptionsState] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [updateResult, setUpdateResult] = useState<null | boolean>(null);

  const { updateModal } = modalSlice.actions;
  const { updateUser } = userSlice.actions;

  useEffect(() => setColor(randomizeColor()), []);

  const determineDate = () => {
    const differenceInMinutes = (Number(new Date()) - Number(new Date(data.date))) / (1000 * 60);
    if (differenceInMinutes < 1) return 'just now';
    else if (differenceInMinutes < 60) return differenceInMinutes.toFixed(0) + ' m';
    else if (differenceInMinutes < 1440) return (differenceInMinutes / 60).toFixed(0) + ' h';
    else if (differenceInMinutes < 43200)
      return (differenceInMinutes / (60 * 24)).toFixed(0) + ' day';
    else if (differenceInMinutes < 518400)
      return (differenceInMinutes / (60 * 24 * 30.44)).toFixed(0) + ' month';
    else return (differenceInMinutes / (60 * 24 * 365)).toFixed(0) + ' year';
  };

  const generateCommentContent = () => {
    const filter: BadWordsFilter = new Filter();
    const filteredText: string = filter.clean(data.text);
    const contentArr = filteredText.split(' ').map((item, wordIndex) => {
      if (item.includes('§'))
        return (
          <span
            key={wordIndex}
            className={cn('comment__link', !dark ? 'light' : 'dark')}
            onClick={() => (location.href = item.replace('§', ''))}
          >
            <FontAwesomeIcon className={'comments__head-linkIcon'} icon={icon({ name: 'link' })} />{' '}
            Web link
          </span>
        );
      else if (item.includes('@'))
        return (
          <span key={wordIndex} className={'comment__reply'}>
            {item}
          </span>
        );
      return item;
    });
    return (
      <div className={cn('comment__content', !dark ? 'light' : 'dark')}>
        {contentArr.map((item) => (typeof item === 'string' ? item + ' ' : item))}
      </div>
    );
  };

  const handleDeleteComment = () => {
    void DELETE_COMMENT(name, data.period, data._id || '').then((result) => {
      void queryClient.invalidateQueries(['cryptoDetail', name]);
      setQueryCommentResult(result.success);
      setOptionsState(false);
    });
  };

  const handleEditComment = () => {
    setEditMode(!editMode);
    setOptionsState(false);
  };

  const handleUpdateComment = async () => {
    setLoadingState(true);
    const newText: string =
      document.querySelector<HTMLTextAreaElement>('#editComment')?.value ?? '';
    await UPDATE_COMMENT(name, data.period, data._id || '', newText || '').then((result) => {
      void queryClient.invalidateQueries(['cryptoDetail', name]);
      setUpdateResult(result.success);
      setQueryCommentResult(result.success);
      setLoadingState(false);
      setEditMode(false);
    });
  };

  const handleUpdateCommentSentiment = async () => {
    if (user.id === data.authorId && !loadingState && queryCommentResult === null) {
      setLoadingState(true);
      await UPDATE_COMMENT_SENTIMENT(
        name,
        data.period,
        data._id || '',
        data.sentiment === 'POSITIVE' ? 'NEGATIVE' : 'POSITIVE',
      ).then((result) => {
        void queryClient.invalidateQueries(['cryptoDetail', name]);
        setUpdateResult(result.success);
        setQueryCommentResult(result.success);
        setLoadingState(false);
      });
    }
  };

  const handleValidateComment = (e: ChangeEvent<HTMLTextAreaElement>) => {
    let newValue = e.target.value;
    newValue = newValue.replace(/[^a-zA-Z0-9\s!@#$§%^&*()_+\-=[\]{};':"\\|,.<>/?]+/g, '');
    const textArea = document.querySelector<HTMLTextAreaElement>('#editComment');
    if (textArea) textArea.value = newValue;
  };

  const handleLikeComment = async () => {
    if (user.id) {
      if (!loadingState) {
        setLoadingState(true);
        await UPDATE_LIKES(user.id, data._id || '').then((response) => {
          if (response.rateConflict) {
            void UPDATE_COMMENT_DISLIKES(name, data.period, data._id || '', -1).then(() => {});
          }
          dispatch(
            updateUser({ ...user, likes: response.likes ?? [], dislikes: response.dislikes ?? [] }),
          );
        });
        if (user.likes.includes(data._id ?? '')) {
          await UPDATE_COMMENT_LIKES(name, data.period, data._id || '', -1).then(() => {
            void queryClient.invalidateQueries(['cryptoDetail', name]);
          });
        } else {
          await UPDATE_COMMENT_LIKES(name, data.period, data._id || '', 1).then(() => {
            void queryClient.invalidateQueries(['cryptoDetail', name]);
          });
        }
      }
    } else {
      dispatch(updateModal(true));
    }
    setLoadingState(false);
  };

  const handleDislikeComment = async () => {
    if (user.id) {
      if (!loadingState) {
        setLoadingState(true);
        await UPDATE_DISLIKES(user.id, data._id || '').then((response) => {
          if (response.rateConflict) {
            void UPDATE_COMMENT_LIKES(name, data.period, data._id || '', -1).then(() => {});
          }
          dispatch(
            updateUser({ ...user, likes: response.likes ?? [], dislikes: response.dislikes ?? [] }),
          );
        });
        if (user.dislikes.includes(data._id ?? '')) {
          await UPDATE_COMMENT_DISLIKES(name, data.period, data._id || '', -1).then(() => {
            void queryClient.invalidateQueries(['cryptoDetail', name]);
          });
        } else {
          await UPDATE_COMMENT_DISLIKES(name, data.period, data._id || '', 1).then(() => {
            void queryClient.invalidateQueries(['cryptoDetail', name]);
          });
        }
      }
    } else {
      dispatch(updateModal(true));
    }
    setLoadingState(false);
  };

  useEffect(() => {
    if (updateResult !== null) setTimeout(() => setUpdateResult(null), 1500);
  }, [updateResult]);

  return (
    <div className={cn('comment', !dark ? 'light' : 'dark')}>
      <div className={'comment__name-line'}>
        <div style={{ backgroundColor: color }} className={'comment__name-img'}>
          {data.author.split(' ').reduce((prev, current) => prev + current[0], '')}
        </div>
        <p className={cn('comment__name', !dark ? 'light' : 'dark')}>{data.author}</p>
        <p className={cn('comment__username', !dark ? 'light' : 'dark')}>@{data.authorUsername}</p>
        <div className={cn('comment__dot', !dark ? 'light' : 'dark')} />
        <p className={cn('comment__date', !dark ? 'light' : 'dark')}>{determineDate()}</p>
        {data.sentiment !== 'NEUTRAL' && (
          <div
            onClick={handleUpdateCommentSentiment}
            style={{
              cursor:
                user.id === data.authorId
                  ? loadingState || queryCommentResult !== null
                    ? 'not-allowed'
                    : 'pointer'
                  : 'default',
            }}
            className={cn(
              'comment__sentiment',
              data.sentiment === 'POSITIVE'
                ? 'comment__sentiment-positive'
                : 'comment__sentiment-negative',
            )}
          >
            {data.sentiment === 'POSITIVE' ? (
              <>
                Bullish
                <FontAwesomeIcon
                  className={'arrow-sort arrow-sort-revert comment-arrow-up'}
                  icon={icon({ name: 'sort-up' })}
                />
              </>
            ) : (
              <>
                Bearish
                <FontAwesomeIcon
                  className={'arrow-sort comment-arrow-down'}
                  icon={icon({ name: 'sort-down' })}
                />
              </>
            )}
          </div>
        )}
      </div>
      {data.repliedToCommentId && data.repliedToAuthorUsername && (
        <div className={cn('comment__replied', !dark ? 'light' : 'dark')}>
          <FontAwesomeIcon
            className={cn('comment__replied-icon', !dark ? 'light' : 'dark')}
            icon={icon({ name: 'reply' })}
          />
          <p>
            {t('repliedTo')} <span>@{data.repliedToAuthorUsername}</span>
          </p>
        </div>
      )}
      {editMode ? (
        <div className={'comment__edit-box'}>
          <textarea
            id={'editComment'}
            onChange={handleValidateComment}
            style={{
              borderColor: updateResult !== null ? (updateResult ? '#41FFD1' : '#F94772') : '',
            }}
            rows={4}
            maxLength={500}
            placeholder={t('updateCommentPlaceholder')}
            className={cn('comment__content comment__textarea', !dark ? 'light' : 'dark')}
            defaultValue={data.text}
          />
          <Button
            disabled={loadingState || queryCommentResult !== null}
            onClick={handleUpdateComment}
            className={'comments__new-comment-post'}
            primary
          >
            {t('btnUpdateComment')}
            {updateResult !== null ? (
              updateResult ? (
                <FontAwesomeIcon className={'comments__tickIcon'} icon={icon({ name: 'check' })} />
              ) : (
                <FontAwesomeIcon className={'comments__xmarkIcon'} icon={icon({ name: 'xmark' })} />
              )
            ) : null}
          </Button>
        </div>
      ) : (
        generateCommentContent()
      )}
      <div className={cn('comment__actions-bar', !dark ? 'light' : 'dark')}>
        <div
          onClick={handleLikeComment}
          className={cn(
            'comment__actions-bar-item',
            user.likes.includes(data._id ?? '') ? 'comment__rated' : '',
          )}
        >
          <FontAwesomeIcon className={'comment__action'} icon={icon({ name: 'thumbs-up' })} />
          <p>{data.likes ?? 0}</p>
        </div>
        <div
          onClick={handleDislikeComment}
          className={cn(
            'comment__actions-bar-item',
            user.dislikes.includes(data._id ?? '') ? 'comment__rated' : '',
          )}
        >
          <FontAwesomeIcon className={'comment__action'} icon={icon({ name: 'thumbs-down' })} />
          <p>{data.dislikes ?? 0}</p>
        </div>
        <div
          onClick={() =>
            setReplyState({
              authorUsername: data.authorUsername,
              id: data._id || '',
              period: data.period,
            })
          }
          className={'comment__actions-bar-item'}
        >
          <FontAwesomeIcon className={'comment__action'} icon={icon({ name: 'reply' })} />
          <p>{data.replies ?? 0}</p>
        </div>
        <FontAwesomeIcon
          className={'comment__action comment__options-button'}
          icon={icon({ name: 'ellipsis' })}
          onClick={() => setOptionsState(!optionsState)}
        />
        {optionsState && (
          <div className={cn('comment__option-box', !dark ? 'light' : 'dark')}>
            {user.id === data.authorId ? (
              <>
                <p onClick={() => handleEditComment()}>
                  {editMode ? t('cancelOption') : t('editOption')}
                </p>
                <p onClick={() => handleDeleteComment()}>{t('deleteOption')}</p>
              </>
            ) : (
              <p className={'comment__option-box__no-options'}>{t('noOptions')}</p>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default Comment;
