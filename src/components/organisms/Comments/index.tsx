import React, { ReactElement, useEffect, useMemo, useRef, useState } from 'react';
import cn from 'classnames';
import { useQueryClient } from '@tanstack/react-query';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from 'react-i18next';

import Comment from './Comment';
import useWindowWidth from 'hooks/useWindowWidth';
import capitalize from 'utils/capitalize';
import scrollToTop from 'utils/srollToTop';
import { Button, ControlledInput, Loader } from 'components/atoms';
import { POST_COMMENT } from 'queries';
import { useAppSelector } from 'hooks/useAppSelector';

import { ICryptoDetailData } from 'queries/globalData/GET_CRYPTOS_LIST_DATA';
import { CommentType } from 'queries/cryptoDetail/POST_COMMENT';

import './Comments.scss';

export type ReplyStateType = {
  authorUsername: string;
  id: string;
  period: number;
};

type CommentsPropType = {
  data: ICryptoDetailData;
  name: string;
};

const Comments = ({ name, data }: CommentsPropType): ReactElement => {
  const { t } = useTranslation('Comments');

  const { dark } = useAppSelector((state) => state.themeReducer);
  const { user } = useAppSelector((state) => state.userReducer);

  const queryClient = useQueryClient();

  const newComment = useRef() as React.MutableRefObject<HTMLInputElement>;

  const windowWidth = useWindowWidth();

  const [queryCommentResult, setQueryCommentResult] = useState<null | boolean>(null);
  const [inputFocusState, setInputFocusState] = useState<boolean>(false);
  const [mouseOnInputBox, setMouseOnInputBox] = useState<boolean>(false);
  const [comments, setComments] = useState<Array<CommentType>>([]);
  const [clearInput, setClearInput] = useState<boolean>(false);
  const [replyState, setReplyState] = useState<ReplyStateType | null>(null);
  const [loadingState, setLoadingState] = useState<boolean>(false);
  const [accountsListState, setAccountsListState] = useState<boolean>(false);
  const [updateInputValue, setUpdateInputValue] = useState<string>('');

  useEffect(() => {
    const allSortedCommentsArr = data.data1D.comments
      .concat(data.data7D.comments, data.data30D.comments)
      .sort(
        (firstComment, secondComment) =>
          Number(new Date(secondComment.date)) - Number(new Date(firstComment.date)),
      );
    if (
      allSortedCommentsArr.length !== comments.length ||
      JSON.stringify(allSortedCommentsArr) !== JSON.stringify(comments)
    ) {
      setComments(allSortedCommentsArr);
    }
  }, [data]);

  const validateCommentContent = (comment: string) =>
    comment
      .split('§')
      .map((item, index) => (index ? (item.substring(0, 4) === 'http' ? '§' + item : '') : item))
      .filter((item) => item)
      .reduce((prev, current) => prev + current, '')
      .replaceAll(/(?<!§)http/g, '§http');

  const handlePostNewComment = async () => {
    setLoadingState(true);
    const validatedCommentContent = validateCommentContent(newComment.current.value);
    const numbers = [1, 7, 30];
    const today = new Date();
    const commentData = {
      author: user.name,
      authorUsername: user.username,
      text: validatedCommentContent,
      date: today.toLocaleString(),
      authorId:
        user.id ||
        'unknown' + Math.floor(Math.random() * (9999999999 - 1000000000 + 1)) + 1000000000,
      period: numbers[Math.floor(Math.random() * numbers.length)],
      name: name,
      likes: 0,
      dislikes: 0,
      replies: 0,
      sentiment: '',
      repliedToAuthorUsername: replyState?.authorUsername || null,
      repliedToCommentId: replyState?.id || null,
      repliedToPeriod: replyState?.period || null,
    };
    if (commentData.text) {
      await POST_COMMENT(name, commentData).then((result) => {
        void queryClient.invalidateQueries(['cryptoDetail', name]);
        setLoadingState(false);
        setQueryCommentResult(result.success);
        setClearInput(true);
        setReplyState(null);
      });
    } else {
      setQueryCommentResult(false);
      setLoadingState(false);
    }
  };

  const checkReplySymbol = () => {
    if (newComment.current?.value[newComment.current?.value.length - 1] === '@') {
      setAccountsListState(true);
    } else {
      setAccountsListState(false);
    }
  };

  const generateAccountsList = useMemo(
    () =>
      comments
        .map((item) => item.authorUsername)
        .filter((item, index, self) => self.indexOf(item) === index)
        .filter((item) => item !== 'unknown'),
    [comments],
  );

  useEffect(() => {
    if (queryCommentResult !== null) setTimeout(() => setQueryCommentResult(null), 1500);
  }, [queryCommentResult]);

  return (
    <div className={'comments'}>
      {data ? (
        <>
          <div className={cn('comments__head', !dark ? 'light' : 'dark')}>
            <div className={cn('comments__head-title', !dark ? 'light' : 'dark')}>
              <img
                src={require(`cryptocurrency-icons/32/color/${data.icon}.png`)}
                alt={data.icon}
              />
              {name.length === 3 ? name.toUpperCase() : capitalize(name)}
              <div>
                {loadingState && windowWidth <= 576 && (
                  <Loader
                    className={'comments__head-links-queryStatus'}
                    width={16}
                    height={16}
                    margin={false}
                  />
                )}
                {queryCommentResult !== null && windowWidth <= 576 ? (
                  queryCommentResult ? (
                    <FontAwesomeIcon
                      className={'comments__tickIcon comments__head-links-queryStatus'}
                      icon={icon({ name: 'check' })}
                    />
                  ) : (
                    <FontAwesomeIcon
                      className={'comments__xmarkIcon comments__head-links-queryStatus'}
                      icon={icon({ name: 'xmark' })}
                    />
                  )
                ) : null}
              </div>
            </div>
            <div className={'comments__head-links'}>
              <Button
                className={'comments__head-link'}
                onClick={() => (location.href = data.website)}
              >
                <FontAwesomeIcon
                  className={'comments__head-linkIcon'}
                  icon={icon({ name: 'link' })}
                />
                {t('btnOfficialWebsite')}
              </Button>
              <Button
                className={cn('comments__head-link reddit', !dark ? 'light' : 'dark')}
                onClick={() => (location.href = data.reddit)}
              >
                <svg
                  className={'comments__head-linkIcon'}
                  width="16"
                  height="16"
                  viewBox="0 0 12 12"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_457_202)">
                    <path
                      className={'comments__head-link-svg'}
                      d="M10.3195 4.76956C9.9679 4.76956 9.65853 4.91487 9.43118 5.14221C8.59446 4.56331 7.46712 4.19065 6.2179 4.15081L6.86712 1.22581L8.93431 1.68987C8.93431 2.19612 9.34681 2.60862 9.85306 2.60862C10.3687 2.60862 10.7835 2.1844 10.7835 1.67815C10.7835 1.1719 10.371 0.747681 9.85306 0.747681C9.49212 0.747681 9.1804 0.965649 9.02571 1.26331L6.7429 0.757056C6.62806 0.726587 6.51556 0.808618 6.48509 0.923462L5.77259 4.14846C4.53275 4.20002 3.41712 4.57268 2.57806 5.15159C2.35071 4.91487 2.02962 4.76956 1.67806 4.76956C0.374934 4.76956 -0.0516288 6.51799 1.14134 7.11565C1.09915 7.30081 1.0804 7.49768 1.0804 7.69456C1.0804 9.65862 3.2929 11.25 6.00931 11.25C8.73743 11.25 10.9499 9.65862 10.9499 7.69456C10.9499 7.49768 10.9288 7.29143 10.8773 7.10627C12.0468 6.50627 11.6156 4.76956 10.3195 4.76956ZM3.03275 7.23987C3.03275 6.72424 3.44525 6.3094 3.96322 6.3094C4.46947 6.3094 4.88197 6.7219 4.88197 7.23987C4.88197 7.74612 4.46947 8.15862 3.96322 8.15862C3.44759 8.16096 3.03275 7.74612 3.03275 7.23987ZM8.0554 9.43127C7.20228 10.2844 4.79525 10.2844 3.94212 9.43127C3.84837 9.34924 3.84837 9.20393 3.94212 9.11018C4.02415 9.02815 4.16946 9.02815 4.2515 9.11018C4.90306 9.77815 7.064 9.78987 7.74368 9.11018C7.82571 9.02815 7.97103 9.02815 8.05306 9.11018C8.14915 9.20393 8.14915 9.34924 8.0554 9.43127ZM8.03665 8.16096C7.5304 8.16096 7.1179 7.74846 7.1179 7.24221C7.1179 6.72659 7.5304 6.31174 8.03665 6.31174C8.55228 6.31174 8.96712 6.72424 8.96712 7.24221C8.96478 7.74612 8.55228 8.16096 8.03665 8.16096Z"
                      fill="#969BA4"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_457_202">
                      <rect width="12" height="12" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                {t('btnReddit')}
              </Button>
              <div className={'comments__head-links__sub-box'}>
                <Button
                  className={'comments__head-top'}
                  onClick={() => {
                    const commentsList = document.querySelector('.comments__list');
                    if (commentsList) scrollToTop(commentsList);
                  }}
                >
                  {t('btnToTop')}
                </Button>
                <div>
                  {loadingState && windowWidth > 576 && (
                    <Loader
                      className={'comments__head-links-queryStatus'}
                      width={16}
                      height={16}
                      margin={false}
                    />
                  )}
                  {queryCommentResult !== null && windowWidth > 576 ? (
                    queryCommentResult ? (
                      <FontAwesomeIcon
                        className={'comments__tickIcon comments__head-links-queryStatus'}
                        icon={icon({ name: 'check' })}
                      />
                    ) : (
                      <FontAwesomeIcon
                        className={'comments__xmarkIcon comments__head-links-queryStatus'}
                        icon={icon({ name: 'xmark' })}
                      />
                    )
                  ) : null}
                </div>
              </div>
            </div>
          </div>
          <div className={'comments__list'}>
            {comments.map((item, index) => (
              <Comment
                key={index}
                name={name}
                data={item}
                setReplyState={setReplyState}
                setLoadingState={setLoadingState}
                setQueryCommentResult={setQueryCommentResult}
                user={user}
                loadingState={loadingState}
                queryCommentResult={queryCommentResult}
              />
            ))}
            {comments.length === 0 && (
              <div className={cn('comments__empty-list', !dark ? 'light' : 'dark')}>
                {t('noCommentDescription') + '✌🏻'}
              </div>
            )}
          </div>
          <div
            className={cn('comments__new-comment-box', !dark ? 'light' : 'dark')}
            onMouseEnter={() => setMouseOnInputBox(true)}
            onMouseLeave={() => setMouseOnInputBox(false)}
            onClick={() =>
              document.querySelector<HTMLInputElement>('.comments__new-comment')?.focus()
            }
          >
            {replyState && (
              <div className={cn('comments__new-comment__reply', !dark ? 'light' : 'dark')}>
                <div className={'comments__new-comment__reply-subBox'}>
                  <FontAwesomeIcon
                    className={'comments__new-comment__reply-icon'}
                    icon={icon({ name: 'reply' })}
                  />
                  <p>
                    {t('repliedToUser')} <span>@{replyState.authorUsername}</span>
                  </p>
                </div>
                <FontAwesomeIcon
                  className={'comments__new-comment__reply-xmark'}
                  onClick={() => {
                    setReplyState(null);
                    setMouseOnInputBox(false);
                  }}
                  icon={icon({ name: 'xmark' })}
                />
              </div>
            )}
            {accountsListState && (
              <div
                style={{ bottom: replyState ? '84px' : '40px' }}
                className={cn('comments__new-comment__accountsList', !dark ? 'light' : 'dark')}
              >
                {generateAccountsList.map((item, index) => (
                  <p onClick={() => setUpdateInputValue(item)} key={index}>
                    @{item}
                  </p>
                ))}
                {!generateAccountsList.length && (
                  <p
                    className={cn(
                      'comments__new-comment__accountsList-empty',
                      !dark ? 'light' : 'dark',
                    )}
                  >
                    {t('noRegisteredUserComments')}
                  </p>
                )}
              </div>
            )}
            <ControlledInput
              englishOnly
              clearInput={clearInput}
              setClear={setClearInput}
              ref={newComment}
              update={(val) => {
                newComment.current.value = val;
                checkReplySymbol();
              }}
              style={{
                borderColor:
                  queryCommentResult !== null ? (queryCommentResult ? '#41FFD1' : '#F94772') : '',
              }}
              updateInputValue={updateInputValue}
              setUpdateInputValue={setUpdateInputValue}
              onFocus={() => setInputFocusState(true)}
              onBlur={() => setTimeout(() => !mouseOnInputBox && setInputFocusState(false), 100)}
              maxLength={500}
              placeholder={t('newCommentPlaceholder')}
              autoComplete={'off'}
              className={cn('comments__new-comment', !dark ? 'light' : 'dark')}
            />
            {(inputFocusState || windowWidth <= 992) && (
              <div className={cn('comments__new-comment__control', !dark ? 'light' : 'dark')}>
                <Button onClick={() => setUpdateInputValue('@')}>
                  {' '}
                  <FontAwesomeIcon
                    className={'comments__head-atIcon'}
                    icon={icon({ name: 'at' })}
                  />
                </Button>
                <Button onClick={() => setUpdateInputValue('§')}>
                  {' '}
                  <FontAwesomeIcon
                    className={'comments__head-linkIcon comments__head-linkIcon-b'}
                    icon={icon({ name: 'link' })}
                  />
                </Button>
                <Button
                  disabled={loadingState || queryCommentResult !== null}
                  onClick={() => handlePostNewComment()}
                  className={'comments__new-comment-post'}
                  primary
                >
                  {t('btnPostToCommunity')}
                  {queryCommentResult !== null ? (
                    queryCommentResult ? (
                      <FontAwesomeIcon
                        className={'comments__tickIcon'}
                        icon={icon({ name: 'check' })}
                      />
                    ) : (
                      <FontAwesomeIcon
                        className={'comments__xmarkIcon'}
                        icon={icon({ name: 'xmark' })}
                      />
                    )
                  ) : null}
                </Button>
              </div>
            )}
          </div>
        </>
      ) : (
        <Loader width={50} height={50} />
      )}
    </div>
  );
};

export default Comments;
