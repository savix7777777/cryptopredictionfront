import React, { JSX, memo, ReactElement } from 'react';
import cn from 'classnames';
import { useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';

import { useAppSelector } from 'hooks/useAppSelector';
import { Loader } from 'components/atoms';

import './ItemsList.scss';

type ItemsListProps = {
  rows: Array<Array<string | number | JSX.Element>> | undefined;
  columns: Array<string>;
  sortBy: Array<string> | string;
  handleSort: (columnIndex: number) => void;
  sortedByColumn: { column: number | null; direction: number };
  searchedValue: string;
  rowClickFunc: (item: Array<string | number | JSX.Element>) => void;
  activeFilters?: boolean | undefined;
};

const ItemsList = ({
  rows,
  columns,
  sortBy,
  handleSort,
  sortedByColumn,
  searchedValue,
  rowClickFunc,
  activeFilters,
}: ItemsListProps): ReactElement => {
  const { t } = useTranslation('ItemsList');

  const { dark } = useAppSelector((state) => state.themeReducer);

  const renderSortArrow = (direction: number) => {
    switch (direction) {
      case -1:
        return (
          <FontAwesomeIcon
            className={'arrow-sort arrow-sort-revert'}
            icon={icon({ name: 'sort-up' })}
          />
        );
      case 1:
        return <FontAwesomeIcon className={'arrow-sort'} icon={icon({ name: 'sort-down' })} />;
      case 0:
        return <></>;
      default:
        return <></>;
    }
  };

  return (
    <>
      <table className={cn('items-list', !dark ? 'light' : 'dark')}>
        <thead className={cn(!dark ? 'light' : 'dark')}>
          <tr className={'items-list__titles-row'}>
            {columns.map((item, index) => {
              let sortItem = false;
              if (rows?.length && typeof rows[0][index] !== 'object' && item) {
                if (sortBy === 'All') {
                  sortItem = true;
                } else if (typeof sortBy === 'object') {
                  sortItem = sortBy.includes(item);
                }
              }
              return (
                <th
                  className={cn(
                    rows?.length && typeof rows[0][index] === 'object' && 'items-list__jsx-content',
                    !dark ? 'light' : 'dark',
                  )}
                  style={{ cursor: sortItem ? 'pointer' : 'default' }}
                  onClick={() => sortItem && handleSort(index)}
                  scope={'col'}
                  key={index}
                >
                  {item + ' '}
                  {sortedByColumn.column === index ? (
                    renderSortArrow(sortedByColumn.direction)
                  ) : (
                    <></>
                  )}
                </th>
              );
            })}
          </tr>
        </thead>
        {rows && rows.length ? (
          <tbody>
            {rows.map((item, index) => (
              <tr
                onClick={() => rowClickFunc(item)}
                className={cn('items-list__row', !dark ? 'light' : 'dark')}
                key={index}
              >
                {item.map((elem, indexI) => (
                  <td
                    className={cn(
                      rows?.length &&
                        typeof rows[index][indexI] === 'object' &&
                        'items-list__jsx-content-cell',
                    )}
                    key={indexI}
                  >
                    {typeof elem === 'object' ? elem : String(elem)}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        ) : (
          <></>
        )}
      </table>

      {!rows?.length ? (
        searchedValue ? (
          <div className={cn('items-list__no-matches', !dark ? 'light' : 'dark')}>
            {t('no_matches')}
          </div>
        ) : activeFilters ? (
          <div className={cn('items-list__no-matches', !dark ? 'light' : 'dark')}>
            {t('no_cryptos')}
          </div>
        ) : (
          <Loader width={50} height={50} />
        )
      ) : (
        <></>
      )}
    </>
  );
};

export default memo(ItemsList);
