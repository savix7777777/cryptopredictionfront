import React, { JSX, memo, ReactElement, useCallback, useEffect, useState } from 'react';
import cn from 'classnames';
import { useTranslation } from 'react-i18next';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import ItemsList from './ItemsList';
import { Input } from 'components/atoms';
import { isNaNCheck, preventNaN } from 'utils/isNaNCheck';
import { useAppSelector } from 'hooks/useAppSelector';

import './SearchList.scss';

type SearchListProps = {
  rows: Array<Array<string | number | JSX.Element>>;
  columns: Array<string>;
  searchBy: Array<string> | string;
  sortBy: Array<string> | string;
  rowClickFunc: (item: Array<string | number | JSX.Element>) => void;
  filtersBox?: boolean;
  activeFilters?: boolean;
  mainColumn?: number;
  className?: string;
};

const SearchList = ({
  className,
  rows,
  columns,
  searchBy,
  sortBy,
  rowClickFunc,
  activeFilters,
  filtersBox = false,
}: SearchListProps): ReactElement => {
  const [rowsData, setRowsData] = useState<Array<Array<string | number | JSX.Element>> | undefined>(
    undefined,
  );
  const [searchedValue, setSearchedValue] = useState('');
  const [sortedByColumn, setSortedByColumn] = useState<{
    column: number | null;
    direction: number;
  }>({ column: null, direction: 0 });
  const [firstRender, setFirstRender] = useState(true);

  const { t } = useTranslation('SearchList');

  const { dark } = useAppSelector((state) => state.themeReducer);

  const transformPropsToIndexes = (prop: Array<string> | string) => {
    let indexes: Array<number> = [];

    if (prop === 'All') {
      indexes = columns
        .map((item) => columns.findIndex((elem) => elem === item))
        .filter((item) => item >= 0);
    } else if (typeof prop === 'object') {
      indexes = prop
        .map((item) => columns.findIndex((elem) => elem === item))
        .filter((item) => item >= 0);
    }

    return indexes;
  };

  const handleSort = useCallback((columnIndex: number) => {
    setSortedByColumn((prevState) => {
      if (columnIndex !== prevState.column) {
        return { column: columnIndex, direction: 1 };
      } else {
        let newDirection = 1;
        switch (prevState.direction) {
          case 0:
            newDirection = 1;
            break;
          case 1:
            newDirection = -1;
            break;
          case -1:
            newDirection = 0;
            break;
          default:
            newDirection = 1;
            break;
        }
        return { column: columnIndex, direction: newDirection };
      }
    });
  }, []);

  const sortFunction = (unsortedRowsData: Array<Array<string | number | JSX.Element>>) => {
    if (sortedByColumn.column !== null) {
      let newRows: Array<Array<string | number | JSX.Element>>;
      if (sortedByColumn.direction && unsortedRowsData) {
        newRows = unsortedRowsData?.slice();
        newRows?.sort((a, b): number => {
          if (sortedByColumn.column !== null) {
            const aItem = a[sortedByColumn.column];
            const bItem = b[sortedByColumn.column];
            if (sortedByColumn.direction > 0) {
              if (isNaNCheck(aItem) || isNaNCheck(bItem)) {
                return String(aItem).toLowerCase().localeCompare(String(bItem).toLowerCase());
              } else {
                return preventNaN(bItem) - preventNaN(aItem);
              }
            } else {
              if (isNaNCheck(aItem) || isNaNCheck(bItem)) {
                return String(bItem).toLowerCase().localeCompare(String(aItem).toLowerCase());
              } else {
                return preventNaN(aItem) - preventNaN(bItem);
              }
            }
          }
          return 0;
        });
      } else newRows = unsortedRowsData;

      return newRows;
    }
    return unsortedRowsData;
  };

  useEffect(() => {
    const indexes = transformPropsToIndexes(searchBy);

    const newRows = rows.filter((item) => {
      let found = false;
      indexes.forEach((elem) => {
        if (
          String(item[elem]).toLowerCase().includes(searchedValue.toLowerCase()) &&
          typeof item[elem] !== 'object'
        )
          found = true;
      });
      return found;
    });

    setRowsData(sortFunction(newRows));
  }, [searchedValue]);

  useEffect(() => {
    if (sortedByColumn.column !== null) {
      let newRows: Array<Array<string | number | JSX.Element>>;
      if (sortedByColumn.direction && rowsData) newRows = sortFunction(rowsData);
      else {
        newRows = rows.filter((item) => {
          let found = false;
          rowsData?.forEach((elem) => {
            if (elem[0] === item[0]) found = true;
          });
          return found;
        });
      }

      setRowsData(newRows);
    }
  }, [sortedByColumn]);

  useEffect(() => {
    setRowsData(() => {
      if (rows.length) {
        if (firstRender) {
          setFirstRender(false);
          return rows;
        } else {
          let filteredRows = rows;
          if (searchedValue) {
            const indexes = transformPropsToIndexes(searchBy);

            filteredRows = rows.filter((item) => {
              let found = false;
              indexes.forEach((elem) => {
                if (
                  String(item[elem]).toLowerCase().includes(searchedValue.toLowerCase()) &&
                  typeof item[elem] !== 'object'
                )
                  found = true;
              });
              return found;
            });
          }
          return sortFunction(filteredRows);
        }
      }
    });
  }, [rows]);

  return (
    <div className={cn('search-list', className)}>
      <div className={'search-list__control-box'}>
        <div
          className={cn('search-list__search-box', !filtersBox && 'search-list__search-box-full')}
        >
          <Input
            className={cn('search-list__input', !dark ? 'light' : 'dark')}
            placeholder={t('searchPlaceholder')}
            value={searchedValue}
            onChange={({ target }) => setSearchedValue(target.value)}
          />
          <FontAwesomeIcon
            className={cn('search-list__search-icon', !dark ? 'light' : 'dark')}
            icon={icon({ name: 'magnifying-glass' })}
          />
        </div>
        {filtersBox && <div className={'search-list__filters-box'} />}
      </div>
      <div className={'search-list__table-box'}>
        <ItemsList
          rows={rowsData}
          columns={columns}
          sortBy={sortBy}
          handleSort={handleSort}
          sortedByColumn={sortedByColumn}
          searchedValue={searchedValue}
          rowClickFunc={rowClickFunc}
          activeFilters={activeFilters}
        />
      </div>
    </div>
  );
};

export default memo(SearchList);
