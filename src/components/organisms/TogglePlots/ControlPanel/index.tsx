import React, { Dispatch, ReactElement, SetStateAction, useEffect, useState } from 'react';
import MultiSwitch from '../../../molecules/MultiSwitch';
import { useTranslation } from 'react-i18next';

import { useAppSelector } from 'hooks/useAppSelector';

import { PlotsConfigType } from '../index';

import './ControlPanel.scss';
import useWindowWidth from '../../../../hooks/useWindowWidth';

type ControlPanelType = {
  config: PlotsConfigType;
  setConfig: Dispatch<SetStateAction<PlotsConfigType>>;
};

const ControlPanel = ({ config, setConfig }: ControlPanelType): ReactElement => {
  const [disabledSwitch, setDisabledSwitch] = useState<boolean>(false);

  const { t } = useTranslation('ControlPanelToggle');
  const windowWidth = useWindowWidth();

  const { dark } = useAppSelector((state) => state.themeReducer);

  useEffect(() => {
    setDisabledSwitch(true);
    setTimeout(() => setDisabledSwitch(false), 1500);
  }, []);

  return (
    <div className={'toggle-plots__control'}>
      <div className={'toggle-plots__control-plotsType'}>
        {disabledSwitch && <div className={'toggle-plots__control-block'} />}
        <MultiSwitch
          texts={[t('futurePredictions'), t('predictedActual')]}
          selectedSwitch={config.plotType}
          bgColor={!dark ? '#eeeff1' : '#37373D'}
          borderWidth={'0px'}
          onToggleCallback={(selectedItem: number) => {
            setConfig({ ...config, plotType: selectedItem });
            setDisabledSwitch(true);
            setTimeout(() => setDisabledSwitch(false), 1500);
          }}
          fontColor={!dark ? '#929292' : '#C4C8D0'}
          selectedSwitchColor={'#3EAEFF'}
          selectedFontColor={'#fff'}
          multiSwitchWidth={windowWidth <= 576 ? '100%' : '340px'}
          height={windowWidth <= 460 ? '32px' : '36px'}
          fontSize={windowWidth <= 460 ? '12px' : '14px'}
          fontWeight={'700'}
        />
      </div>
      <div className={'toggle-plots__control-days'}>
        {disabledSwitch && <div className={'toggle-plots__control-block'} />}
        <MultiSwitch
          texts={['1D', '7D', '1M']}
          selectedSwitch={config.days}
          bgColor={!dark ? '#eeeff1' : '#37373D'}
          borderWidth={'0px'}
          onToggleCallback={(selectedItem: number) => {
            setConfig({ ...config, days: selectedItem });
            setDisabledSwitch(true);
            setTimeout(() => setDisabledSwitch(false), 1500);
          }}
          fontColor={!dark ? '#929292' : '#C4C8D0'}
          selectedSwitchColor={'#3EAEFF'}
          selectedFontColor={'#fff'}
          multiSwitchWidth={windowWidth <= 576 ? '100%' : '128px'}
          height={windowWidth <= 460 ? '32px' : '36px'}
          fontSize={windowWidth <= 460 ? '12px' : '14px'}
          fontWeight={'700'}
        />
      </div>
    </div>
  );
};

export default ControlPanel;
