import React, { memo, ReactElement, useMemo, useState } from 'react';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from 'recharts';
import cn from 'classnames';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from 'react-i18next';

import ControlPanel from './ControlPanel';
import useWindowWidth from 'hooks/useWindowWidth';
import { useAppSelector } from 'hooks/useAppSelector';
import { Loader } from 'components/atoms';
import { GET_CRYPTO_PRICE_PER_PERIOD } from 'queries';
import { getDaysDataInterval } from 'utils/getDaysDataInterval';

import { ICryptoDetailData } from 'queries/globalData/GET_CRYPTOS_LIST_DATA';

import './TogglePlots.scss';

export type PlotsConfigType = {
  plotType: number;
  days: number;
};

type TogglePlotsType = {
  cryptoDetail: ICryptoDetailData;
};

const TogglePlots = ({ cryptoDetail }: TogglePlotsType): ReactElement => {
  const [config, setConfig] = useState<PlotsConfigType>({
    plotType: 0,
    days: 0,
  });

  const [plotData, setPlotData] = useState<Array<{
    date: string;
    price: number;
    prediction: number;
  }> | null>(null);

  const { t } = useTranslation('TogglePlots');

  const windowWidth = useWindowWidth();

  const { dark } = useAppSelector((state) => state.themeReducer);

  void useMemo(async () => {
    let prices: number[] | null;
    if (config.plotType) {
      switch (config.days) {
        case 0:
          prices = (await GET_CRYPTO_PRICE_PER_PERIOD(cryptoDetail.name, 6)).data;
          if (prices) {
            const pricesArray = prices.slice();
            const datesArray = getDaysDataInterval(1, false).reverse();
            const newPlotData = pricesArray.map((item, index) => ({
              date: datesArray[index],
              price: item,
              prediction: cryptoDetail.data1D.predictionsHistory[index] || 0,
            }));
            plotData ? setTimeout(() => setPlotData(newPlotData), 1500) : setPlotData(newPlotData);
          }
          break;
        case 1:
          prices = (await GET_CRYPTO_PRICE_PER_PERIOD(cryptoDetail.name, 48)).data;
          if (prices) {
            const pricesArray = prices.slice().reverse();
            const filteredPricesArray = pricesArray.filter(
              (item, index) => index === 0 || index % 7 === 0,
            );
            const datesArray = getDaysDataInterval(7, false);
            const filteredPredictionsArray = cryptoDetail.data7D.predictionsHistory
              .slice()
              .reverse()
              .filter((item, index) => index === 0 || index % 7 === 0);
            filteredPredictionsArray.shift();
            const newPlotData = filteredPricesArray
              .map((item, index) => ({
                date: datesArray[index],
                price: item,
                prediction: filteredPredictionsArray[index] || 0,
              }))
              .reverse();
            plotData ? setTimeout(() => setPlotData(newPlotData), 1500) : setPlotData(newPlotData);
          }
          break;
        case 2:
          prices = (await GET_CRYPTO_PRICE_PER_PERIOD(cryptoDetail.name, 209)).data;
          if (prices) {
            const pricesArray = prices.slice().reverse();
            const filteredPricesArray = pricesArray.filter(
              (item, index) => index === 0 || index % 30 === 0,
            );
            const datesArray = getDaysDataInterval(30, false);
            const filteredPredictionsArray = cryptoDetail.data30D.predictionsHistory
              .slice()
              .reverse()
              .filter((item, index) => index === 0 || index % 7 === 0);
            filteredPredictionsArray.shift();
            const newPlotData = filteredPricesArray
              .map((item, index) => ({
                date: datesArray[index],
                price: item,
                prediction: filteredPredictionsArray[index] || 0,
              }))
              .reverse();
            plotData ? setTimeout(() => setPlotData(newPlotData), 1500) : setPlotData(newPlotData);
          }
          break;
        default:
          location.reload();
      }
    } else {
      switch (config.days) {
        case 0:
          prices = (await GET_CRYPTO_PRICE_PER_PERIOD(cryptoDetail.name, 6)).data;
          if (prices) {
            const pricesArray = prices.slice();
            pricesArray.pop();
            pricesArray.push(+cryptoDetail.data1D.price.toFixed(2));
            const datesArray = getDaysDataInterval(1, false).reverse();
            const newPlotData = pricesArray.map((item, index) => ({
              date: datesArray[index],
              price: item,
              prediction: item,
            }));
            plotData ? setTimeout(() => setPlotData(newPlotData), 1500) : setPlotData(newPlotData);
          }
          break;
        case 1:
          prices = (await GET_CRYPTO_PRICE_PER_PERIOD(cryptoDetail.name, 48)).data;
          if (prices) {
            const pricesArray = prices.slice().reverse();
            const filteredPricesArray = pricesArray.filter(
              (item, index) => index === 0 || index % 7 === 0,
            );
            filteredPricesArray.pop();
            filteredPricesArray.unshift(+cryptoDetail.data7D.price.toFixed(2));
            const datesArray = getDaysDataInterval(7, true);
            const newPlotData = filteredPricesArray
              .map((item, index) => ({
                date: datesArray[index],
                price: item,
                prediction: item,
              }))
              .reverse();
            plotData ? setTimeout(() => setPlotData(newPlotData), 1500) : setPlotData(newPlotData);
          }
          break;
        case 2:
          prices = (await GET_CRYPTO_PRICE_PER_PERIOD(cryptoDetail.name, 209)).data;
          if (prices) {
            const pricesArray = prices.slice().reverse();
            const filteredPricesArray = pricesArray.filter(
              (item, index) => index === 0 || index % 30 === 0,
            );
            filteredPricesArray.pop();
            filteredPricesArray.unshift(+cryptoDetail.data30D.price.toFixed(2));
            const datesArray = getDaysDataInterval(30, true);
            const newPlotData = filteredPricesArray
              .map((item, index) => ({
                date: datesArray[index],
                price: item,
                prediction: item,
              }))
              .reverse();
            plotData ? setTimeout(() => setPlotData(newPlotData), 1500) : setPlotData(newPlotData);
          }
          break;
        default:
          location.reload();
      }
    }
  }, [config]);

  return (
    <div className={'toggle-plots'}>
      <div className={'toggle-plots__hint'}>
        <div className={cn('toggle-plots__hint-title', !dark ? 'light' : 'dark')}>
          {config.plotType ? (
            <div>
              <FontAwesomeIcon
                className={'toggle-plots__hint-icon'}
                icon={icon({ name: 'chart-column' })}
              />
              {t('predicted')} / {t('actual')}
            </div>
          ) : (
            <div>
              <FontAwesomeIcon
                className={'toggle-plots__hint-icon'}
                icon={icon({ name: 'chart-line' })}
              />
              {t('futurePredictionsTitle')}
            </div>
          )}
        </div>
        <p className={cn('toggle-plots__hint-text', !dark ? 'light' : 'dark')}>
          {config.plotType ? t('predictedActualDescription') : t('futurePredictionsDescription')}
        </p>
      </div>
      <div className={'toggle-plots__control-wrapper'}>
        <ControlPanel setConfig={setConfig} config={config} />
      </div>
      {plotData ? (
        <>
          <ResponsiveContainer width={windowWidth <= 650 ? '100%' : '95%'} height={200}>
            <LineChart
              data={plotData}
              margin={{
                top: 0,
                right: 2,
                left: 2,
                bottom: 0,
              }}
            >
              {!config.plotType && (
                <defs>
                  <linearGradient id="gradient" x1="0" y1="0" x2="100%" y2="0">
                    <stop offset="0%" stopColor="#63ABFD" />
                    <stop offset={`83.6%`} stopColor="#63ABFD" />
                    <stop offset={`83.6%`} stopColor="#E697FF" />
                    <stop offset="100%" stopColor="#E697FF" />
                  </linearGradient>
                </defs>
              )}
              <XAxis
                tickLine={false}
                axisLine={false}
                tickMargin={10}
                tick={{ fill: !dark ? '#515151' : '#fff' }}
                fontSize={windowWidth <= 576 ? 12 : 14}
                dataKey="date"
              />
              <YAxis
                domain={[
                  Math.floor(
                    Math.min(
                      ...(plotData
                        ? plotData.map((item) =>
                            item.prediction && item.prediction < item.price
                              ? +item.prediction.toFixed(0)
                              : +item.price.toFixed(0),
                          )
                        : []),
                    ) / 10,
                  ) * 10,
                  Math.ceil(
                    Math.max(
                      ...(plotData
                        ? plotData.map((item) =>
                            item.prediction && item.prediction > item.price
                              ? +item.prediction.toFixed(0)
                              : +item.price.toFixed(0),
                          )
                        : []),
                    ) / 10,
                  ) * 10,
                ]}
                tickLine={false}
                axisLine={false}
                tickMargin={15}
                tick={{ fill: !dark ? '#515151' : '#fff' }}
                fontSize={windowWidth <= 576 ? 12 : 14}
              />
              <Tooltip
                contentStyle={{
                  color: !dark ? '#414247' : '#fff',
                  background: !dark ? '#eeeff1' : '#2C2D33',
                  borderColor: !dark ? '#969BA4' : '#929292',
                  borderRadius: '4px',
                  opacity: '0.8',
                  fontSize: '14px',
                }}
              />
              <CartesianGrid stroke={'#4F4F4F'} />
              <Line
                strokeWidth={3}
                type="monotone"
                dataKey="price"
                stroke={config.plotType ? '#63ABFD' : 'url(#gradient)'}
                dot={true}
              />
              {config.plotType && (
                <Line
                  strokeWidth={3}
                  type="monotone"
                  dataKey="prediction"
                  stroke={'#E697FF'}
                  dot={true}
                />
              )}
            </LineChart>
          </ResponsiveContainer>
          <div className={'toggle-plots__chart-hint'}>
            <div className={cn('toggle-plots__chart-hint-box', !dark ? 'light' : 'dark')}>
              <div className={'toggle-plots__chart-hint-blue'} />
              {t('actual')}
            </div>
            <div className={cn('toggle-plots__chart-hint-box', !dark ? 'light' : 'dark')}>
              <div className={'toggle-plots__chart-hint-purple'} />
              {t('predicted')}
            </div>
          </div>
        </>
      ) : (
        <Loader width={50} height={50} />
      )}
    </div>
  );
};

export default memo(TogglePlots);
