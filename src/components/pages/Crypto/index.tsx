import React, {
  JSX,
  ReactElement,
  useEffect,
  useMemo,
  useState,
  MouseEvent,
  useCallback,
} from 'react';
import cn from 'classnames';
import { createPortal } from 'react-dom';
import { useQuery } from '@tanstack/react-query';
import { useLocation, useNavigate } from 'react-router';
import { LineChart, Line, YAxis } from 'recharts';
import { useTranslation } from 'react-i18next';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import useWindowWidth from 'hooks/useWindowWidth';
import { Loader, ErrorMessage, Button } from 'components/atoms';
import { SearchList } from 'components/organisms';
import { useAppSelector } from 'hooks/useAppSelector';
import { useAppDispatch } from 'hooks/useAppDispatch';
import { userSlice } from 'redux/reducers/UserSlice';
import { modalSlice } from 'redux/reducers/ModalSlice';

import {
  GET_ALL_COMMENTS,
  GET_CRYPTO_CURRENT_PRICE,
  GET_CRYPTO_PRICE_PER_PERIOD,
  GET_CRYPTOS_LIST_DATA,
  UPDATE_WATCHLIST,
} from 'queries';
import meanAccuracy from 'utils/meanAccuracy';
import capitalize from 'utils/capitalize';

import { ICryptoDetailData } from 'queries/globalData/GET_CRYPTOS_LIST_DATA';

import './Crypto.scss';

const Crypto = (): ReactElement => {
  const { data, isLoading, isError, error } = useQuery({
    queryKey: ['cryptos'],
    queryFn: () => GET_CRYPTOS_LIST_DATA(),
  });

  const { data: allComments } = useQuery({
    queryKey: ['allComments'],
    queryFn: () => GET_ALL_COMMENTS(),
  });

  const navigate = useNavigate();
  const location = useLocation();
  const dispatch = useAppDispatch();
  const windowWidth = useWindowWidth();
  const { t } = useTranslation('CryptoPage');

  const [watchListState, setWatchListState] = useState(location.state === 'watchlist');
  const [rowsData, setRowsData] = useState<Array<Array<string | number | JSX.Element>>>([]);
  const [filterPopularState, setFilterPopularState] = useState(false);
  const [filterRecommendationsState, setFilterRecommendationsState] = useState(false);

  const { user } = useAppSelector((state) => state.userReducer);
  const { dark } = useAppSelector((state) => state.themeReducer);
  const { updateUser } = userSlice.actions;
  const { updateModal } = modalSlice.actions;

  const handleUpdateWatchlist = useCallback(
    (event: MouseEvent<SVGSVGElement>, item: ICryptoDetailData) => {
      event.stopPropagation();
      if (user.id) {
        void UPDATE_WATCHLIST(user.id, item.name).then((response) =>
          dispatch(updateUser({ ...user, watchlist: response.watchlist ?? [] })),
        );
      } else {
        dispatch(updateModal(true));
      }
    },
    [dispatch, updateUser, user],
  );

  const handleFilterWatchList = () => {
    if (user.id) {
      setWatchListState(!watchListState);
    } else {
      dispatch(updateModal(true));
    }
  };

  const loadWeekPriceHistory = async (name: string) => {
    const data = await GET_CRYPTO_PRICE_PER_PERIOD(name, 168);

    const newData = data.data?.map((item) => ({
      pv: item,
      amt: item,
    }));

    return newData || [];
  };

  const loadAllWeekPriceHistory = async (names: string[]) => {
    const weekPriceHistoryData: {
      [key: string]: Array<{
        pv: number;
        amt: number;
      }>;
    } = {};
    for (const name of names) {
      weekPriceHistoryData[name] = await loadWeekPriceHistory(name);
    }
    return weekPriceHistoryData;
  };

  const prepareCryptosData = (
    data: Array<ICryptoDetailData>,
    weekPriceHistoryData: {
      [key: string]: Array<{
        pv: number;
        amt: number;
      }>;
    },
  ) =>
    data.map((item, index) => [
      <FontAwesomeIcon
        key={index}
        onClick={(event) => handleUpdateWatchlist(event, item)}
        className={cn(
          'crypto__star',
          user.watchlist?.includes(item.name) ? 'crypto__star-active' : '',
          !dark ? 'light' : 'dark',
        )}
        icon={icon({ name: 'star' })}
      />,
      index + 1,
      <img
        className={'crypto__icon'}
        key={index}
        src={require(`cryptocurrency-icons/128/color/${item.icon}.png`)}
        alt={item.icon}
      />,
      item.name.length === 3 ? item.name.toUpperCase() : capitalize(item.name),
      <p className={'crypto__symbol'} key={index}>
        {item.symbol}
      </p>,
      '-',
      item.data1D?.price?.toFixed(2) || '-',
      item.data7D?.price?.toFixed(2) || '-',
      item.data30D?.price && item.data30D?.price > 0 ? item.data30D?.price?.toFixed(2) : '-',
      meanAccuracy([item.data1D.accuracy, item.data7D.accuracy, item.data30D.accuracy]).toFixed(1) +
        '%' || '-',
      <LineChart
        key={index}
        width={150}
        height={40}
        margin={{ top: 0, right: 0 }}
        className={cn('crypto__plot')}
        data={weekPriceHistoryData[item.name] || []}
      >
        <YAxis hide domain={['dataMin', 'dataMax']} />
        <Line dataKey="pv" stroke="#7F8AF2" type={'monotone'} strokeWidth={2} dot={false} />
      </LineChart>,
    ]);

  const loadCurrentPrices = (prevRowsData: Array<Array<string | number | JSX.Element>>) => {
    if (data?.success) {
      const newRowsData = prevRowsData.slice();

      const promises = Object.values('data' in data && data.data)
        .map((item: ICryptoDetailData, index) =>
          GET_CRYPTO_CURRENT_PRICE(String(item.name)).then((result) =>
            result.data
              ? (newRowsData[index][5] = String(result.data))
              : (newRowsData[index][5] = '-'),
          ),
        )
        .filter((promise) => promise);

      void Promise.all(promises).then(() => setRowsData(newRowsData));
    }
  };

  const filteredRowsData = () => {
    let finalRowsData = rowsData.slice();
    if (watchListState) {
      finalRowsData = finalRowsData.filter(
        (item) => user.watchlist?.includes(String(item[3]).toLowerCase()),
      );
    }
    if (filterPopularState) {
      const nameCountMap: Map<string, number> = new Map();
      allComments?.data?.comments.forEach(({ name }) => {
        if (nameCountMap.has(name)) {
          nameCountMap.set(name, (nameCountMap.get(name) || 0) + 1);
        } else {
          nameCountMap.set(name, 1);
        }
      });

      const topCryptos = Array.from(nameCountMap.entries())
        .sort((first, second) => second[1] - first[1])
        .map((item) => item[0])
        .slice(0, 5);

      finalRowsData = finalRowsData.filter((item) =>
        topCryptos.includes(String(item[3]).toLowerCase()),
      );
    }
    if (filterRecommendationsState) {
      finalRowsData.sort(
        (first, second) =>
          +first[5] / Math.abs(+first[5] - +first[6]) -
          +second[5] / Math.abs(+second[5] - +second[6]),
      );
      finalRowsData = finalRowsData.slice(0, 5);
    }

    return finalRowsData;
  };

  const handleNavigate = (item: Array<string | number | JSX.Element>): void =>
    navigate(`${String(item[3]).toLowerCase()}`);

  useMemo(() => {
    if (data?.success && 'data' in data) {
      void loadAllWeekPriceHistory(
        Object.values('data' in data && data.data).map((item: ICryptoDetailData) => item.name),
      ).then((weekPriceHistoryData) => {
        const preparedRowsData = prepareCryptosData(
          Object.values('data' in data && data.data), // eslint-disable-line
          weekPriceHistoryData,
        );
        loadCurrentPrices(preparedRowsData);
      });
    }
  }, [data, user]);

  useEffect(() => {
    if (location.state === 'watchlist') setWatchListState(true);
  }, [location.state]);

  useEffect(() => {
    const apiIntervalID: NodeJS.Timeout = setInterval(() => {
      loadCurrentPrices(rowsData);
    }, 5000);

    return () => clearInterval(apiIntervalID);
  }, [rowsData]);

  return (
    <section className={cn('section section-center crypto')}>
      <div className={'crypto-title-box'}>
        <h3 className={cn('crypto-title', !dark ? 'light' : 'dark')}>{t('cryptoTitle')}</h3>
        <p className={cn('crypto-title__text', !dark ? 'light' : 'dark')}>{t('cryptoSubtitle')}</p>
      </div>
      {isLoading && <Loader width={50} height={50} />}
      {isError && (
        <ErrorMessage
          errorHint={t('loadingError')}
          errorText={error instanceof Error ? error.message : ''}
        />
      )}
      {data && (
        <div className={'crypto-box'}>
          <SearchList
            rows={filteredRowsData()}
            columns={[
              '',
              '#',
              '',
              t('columnName'),
              '',
              t('columnPrice'),
              t('columnEveningPrediction'),
              t('column7Days'),
              t('column30Days'),
              t('columnMeanAccuracy'),
              t('columnLast7Days'),
            ]}
            searchBy={[t('columnName'), t('columnPrice')]}
            sortBy={'All'}
            className={'crypto__search-list'}
            rowClickFunc={handleNavigate}
            filtersBox={true}
            activeFilters={watchListState || filterPopularState || filterRecommendationsState}
          />
        </div>
      )}
      {createPortal(
        <div className={'crypto__filters-box'}>
          <Button
            onClick={handleFilterWatchList}
            className={cn(
              'crypto__filters-box__button',
              watchListState ? 'crypto__filters-box__button-active' : '',
            )}
          >
            <FontAwesomeIcon className={'crypto__watch-list__star'} icon={icon({ name: 'star' })} />
            {t('watchlist')}
          </Button>
          <Button
            onClick={() => setFilterPopularState(!filterPopularState)}
            className={cn(
              'crypto__filters-box__button',
              filterPopularState ? 'crypto__filters-box__button-active' : '',
            )}
          >
            🔥&nbsp; {t(windowWidth >= 540 ? 'popularCoins' : 'popularCoinsShort')}
          </Button>
          <Button
            onClick={() => setFilterRecommendationsState(!filterRecommendationsState)}
            className={cn(
              'crypto__filters-box__button',
              filterRecommendationsState ? 'crypto__filters-box__button-active' : '',
            )}
          >
            🔥&nbsp; {t(windowWidth >= 540 ? 'recommendations' : 'recommendationsShort')}
          </Button>
        </div>,
        document.querySelector('.search-list__filters-box') || document.body,
      )}
    </section>
  );
};

export default Crypto;
