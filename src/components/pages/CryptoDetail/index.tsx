import React, { ReactElement, useCallback, useEffect, useState } from 'react';
import cn from 'classnames';
import { useParams } from 'react-router';
import { useQuery } from '@tanstack/react-query';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';

import { Comments, TogglePlots } from 'components/organisms';
import { CryptoTitle } from 'components/molecules';
import { Button, ErrorMessage, Loader } from 'components/atoms';
import { useTranslation } from 'react-i18next';
import { useAppSelector } from 'hooks/useAppSelector';
import { useAppDispatch } from 'hooks/useAppDispatch';
import { userSlice } from 'redux/reducers/UserSlice';
import { modalSlice } from 'redux/reducers/ModalSlice';
import {
  GET_CRYPTO_CURRENT_PRICE,
  GET_CRYPTO_DETAIL_DATA,
  UPDATE_SUBSCRIBES,
  UPDATE_WATCHLIST,
} from 'queries';

import { ICryptoDetailData } from 'queries/globalData/GET_CRYPTOS_LIST_DATA';

import './CryptoDetail.scss';

const CryptoDetail = (): ReactElement => {
  const { t } = useTranslation('CryptoDetailPage');

  const { cryptocurrency } = useParams();
  const dispatch = useAppDispatch();

  const { data, isLoading, isError, error } = useQuery({
    queryKey: ['cryptoDetail', cryptocurrency],
    queryFn: () => GET_CRYPTO_DETAIL_DATA(cryptocurrency || ''),
  });

  const [cryptoDetail, setCryptoDetail] = useState<ICryptoDetailData | null>(null);

  const { user } = useAppSelector((state) => state.userReducer);
  const { updateUser } = userSlice.actions;
  const { updateModal } = modalSlice.actions;

  const loadCurrentPrice = (prevCryptoDetail: ICryptoDetailData) => {
    const newCryptoDetail = { ...prevCryptoDetail };
    void GET_CRYPTO_CURRENT_PRICE(cryptocurrency || '').then((result) => {
      if (result.data) {
        newCryptoDetail.currentPrice = String(result.data);
      } else {
        newCryptoDetail.currentPrice = '-';
      }
      setCryptoDetail(newCryptoDetail);
    });
  };

  useEffect(() => {
    if (data?.success && 'data' in data) {
      loadCurrentPrice(data.data);
    }
  }, [data]);

  useEffect(() => {
    if (cryptoDetail) {
      const apiIntervalID: NodeJS.Timeout = setInterval(() => {
        loadCurrentPrice(cryptoDetail);
      }, 5000);
      return () => clearInterval(apiIntervalID);
    }
  }, [cryptoDetail]);

  const handleUpdateWatchlist = useCallback(() => {
    if (user.id && cryptocurrency) {
      void UPDATE_WATCHLIST(user.id, cryptocurrency).then((response) =>
        dispatch(updateUser({ ...user, watchlist: response.watchlist ?? [] })),
      );
    } else {
      dispatch(updateModal(true));
    }
  }, [user, cryptocurrency, dispatch]);

  const handleUpdateSubscribes = useCallback(() => {
    if (user.id && cryptocurrency) {
      void UPDATE_SUBSCRIBES(user.id, cryptocurrency).then((response) =>
        dispatch(updateUser({ ...user, subscribes: response.subscribes ?? [] })),
      );
    } else {
      dispatch(updateModal(true));
    }
  }, [user, cryptocurrency, dispatch]);

  return (
    <section
      className={cn('section crypto-detail', isLoading || isError ? 'crypto-detail__center' : '')}
    >
      {cryptoDetail && cryptocurrency && (
        <>
          <div className={'crypto-detail__left-box'}>
            <CryptoTitle name={cryptocurrency} data={cryptoDetail} />
            <div className={'crypto-detail__actions'}>
              <Button onClick={handleUpdateWatchlist}>
                <FontAwesomeIcon className={'crypto-detail__star'} icon={icon({ name: 'star' })} />{' '}
                {user.watchlist?.includes(cryptocurrency)
                  ? t('removeFromWatchlist')
                  : t('addToWatchlist')}
              </Button>
              <Button onClick={handleUpdateSubscribes}>
                <FontAwesomeIcon className={'crypto-detail__bell'} icon={icon({ name: 'bell' })} />{' '}
                {user.subscribes?.includes(cryptocurrency) ? t('unsubscribe') : t('subscribe')}
              </Button>
            </div>
            <TogglePlots cryptoDetail={cryptoDetail} />
          </div>
          <div className={'crypto-detail__right-box'}>
            <Comments name={cryptocurrency} data={cryptoDetail} />
          </div>
        </>
      )}
      {isLoading && <Loader width={50} height={50} />}
      {isError && (
        <ErrorMessage
          errorHint={`${t('errorLoading')} ${cryptocurrency || 'crypto'} ${t(
            'errorLoading_part2',
          )}`}
          errorText={error instanceof Error ? error.message : ''}
        />
      )}
    </section>
  );
};

export default CryptoDetail;
