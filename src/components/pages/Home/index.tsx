import React, { ReactElement } from 'react';
import cn from 'classnames';
import { useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Carousel, Stats } from 'components/molecules';
import { Title } from 'components/atoms';

import useWindowWidth from 'hooks/useWindowWidth';
import { useAppSelector } from 'hooks/useAppSelector';

import PATHS from 'routes/PATHS';

import fox from 'assets/fox.svg';
import btc from 'assets/btc.svg';
import bnb from 'assets/bnb.svg';
import eth from 'assets/eth.svg';

import './Home.scss';

const Home = (): ReactElement => {
  const navigate = useNavigate();
  const { t } = useTranslation('HomePage');

  const { dark } = useAppSelector((state) => state.themeReducer);
  const windowWidth = useWindowWidth();

  const slidesArr = [t('slide1'), t('slide2'), t('slide3'), t('slide4'), t('slide5')];

  return (
    <section className={'section home'}>
      <div className={'home__left-box'}>
        <Title
          className={cn('home__title', dark ? 'dark' : windowWidth <= 992 ? 'light' : 'dark')}
          text={t('title')}
        />
        {windowWidth > 992 && (
          <div className={cn('home__carousel-box', !dark ? 'light' : 'dark')}>
            <Title
              className={cn('home__carousel-box-title', !dark ? 'light' : 'dark')}
              text={t('carouselTitle') + ' ✨'}
            />
            <Carousel slides={slidesArr} />
            <div
              className={'home__carousel-box-navigate'}
              onClick={() => navigate(`/${PATHS.MAIN}/${PATHS.CRYPTO}`)}
            >
              <span className={cn(!dark ? 'light' : 'dark')}>{t('btnStart')}</span>
              <FontAwesomeIcon
                className={cn('play', !dark ? 'light' : 'dark')}
                icon={icon({ name: 'circle-play' })}
              />
            </div>
          </div>
        )}
      </div>
      <div className={'home__images-box'}>
        <img className={'home__btc'} src={btc} alt={''} />
        <img className={'home__bnb'} src={bnb} alt={''} />
        <img className={'home__eth'} src={eth} alt={''} />
        <img className={'home__fox'} src={fox} alt={''} />
      </div>
      <div className={'home__right-box'}>
        <Stats />
        {windowWidth <= 992 && (
          <div className={cn('home__carousel-box', !dark ? 'light' : 'dark')}>
            <Title
              className={cn('home__carousel-box-title', !dark ? 'light' : 'dark')}
              text={t(windowWidth < 460 ? 'carouselTitleShort' : 'carouselTitle') + ' ✨'}
            />
            <Carousel slides={slidesArr} />
            <div
              className={'home__carousel-box-navigate'}
              onClick={() => navigate(`/${PATHS.MAIN}/${PATHS.CRYPTO}`)}
            >
              <span className={cn(!dark ? 'light' : 'dark')}>
                {t(windowWidth < 360 ? 'btnStartShort' : 'btnStart')}
              </span>
              <FontAwesomeIcon
                className={cn('play', !dark ? 'light' : 'dark')}
                icon={icon({ name: 'circle-play' })}
              />
            </div>
          </div>
        )}
      </div>
    </section>
  );
};

export default Home;
