import React, { ReactElement, useEffect, useState } from 'react';
import cn from 'classnames';
import { useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';
import { SubmitHandler, useForm } from 'react-hook-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';

import { Button, Input, Logo } from 'components/atoms';
import { LOGIN } from 'queries';
import { userSlice } from 'redux/reducers/UserSlice';
import { useAppDispatch } from 'hooks/useAppDispatch';

import PATHS from 'routes/PATHS';

import LogoSizeEnum from 'components/atoms/Logo/types/LogoSizeEnum';

import logo from 'assets/logo-light.svg';

import './Login.scss';

type LoginInputsType = {
  username: string;
  password: string;
};

const Login = (): ReactElement => {
  const { t } = useTranslation('LoginPage');

  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [errorState, setErrorState] = useState(false);

  const dispatch = useAppDispatch();

  const { updateUser } = userSlice.actions;

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<LoginInputsType>();

  const navigate = useNavigate();

  const onLogin: SubmitHandler<LoginInputsType> = (data) => {
    void LOGIN(data.username, data.password).then((result) => {
      if (result.success && result.user) {
        dispatch(updateUser(result.user));
        navigate(`/${PATHS.MAIN}/${PATHS.HOME}`, { replace: true });
      } else {
        setErrorState(true);
      }
    });
  };

  useEffect(() => setErrorState(false), [watch('username'), watch('password')]);

  return (
    <section className={cn('section section-center login')}>
      <div className={'login-header'}>
        <Logo
          onClick={() => navigate(`${PATHS.MAIN}/${PATHS.HOME}`)}
          logo={logo}
          size={LogoSizeEnum.MEDIUM}
        />
      </div>
      <form onSubmit={handleSubmit(onLogin)} className={'login-form'}>
        <h3 className={'login-form-header'}>{t('loginFormHeader')}</h3>
        <label className={'login-label'}>
          {t('usernameOrEmail')}
          <Input
            {...register('username', { required: true })}
            type="text"
            name="username"
            placeholder={t('usernameOrEmailPlaceholder')}
            className={'login-input'}
          />
          {(errors.username || errorState) && (
            <div className={'error-box'}>
              <FontAwesomeIcon
                className={'error-icon'}
                icon={icon({ name: 'circle-exclamation' })}
              />
              <p className={'error-message'}>{t('usernameError')}</p>
            </div>
          )}
        </label>
        <label className={'login-label'}>
          {t('passwordLabel')}
          <p className={'login-label-addon'} onClick={() => navigate(`/${PATHS.RESET_PASSWORD}`)}>
            {t('forgotPassword')}
          </p>
          <div className={'login-input-box'}>
            <Input
              {...register('password', { required: true })}
              type={showPassword ? 'text' : 'password'}
              name="password"
              placeholder={t('passwordPlaceholder')}
              className={'login-input password'}
            />
            {showPassword ? (
              <FontAwesomeIcon
                onClick={() => setShowPassword(!showPassword)}
                className={'login-input-icon'}
                icon={icon({ name: 'eye-slash' })}
              />
            ) : (
              <FontAwesomeIcon
                onClick={() => setShowPassword(!showPassword)}
                className={'login-input-icon'}
                icon={icon({ name: 'eye' })}
              />
            )}
          </div>
          {(errors.password || errorState) && (
            <div className={'error-box'}>
              <FontAwesomeIcon
                className={'error-icon'}
                icon={icon({ name: 'circle-exclamation' })}
              />
              <p className={'error-message'}>{t('passwordError')}</p>
            </div>
          )}
        </label>
        <Button type={'submit'} className={'login-btn'}>
          <span>{t('loginButton')}</span>
        </Button>
        <div className={'login-switch-box'}>
          <p className={'login-switch-text'}>{t('noAccountQuestion')}</p>
          <p className={'login-switch-link'} onClick={() => navigate(`/${PATHS.REGISTER}`)}>
            {t('registerLink')}
          </p>
        </div>
      </form>
    </section>
  );
};

export default Login;
