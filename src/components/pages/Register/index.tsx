import React, { ReactElement, useEffect, useState } from 'react';
import cn from 'classnames';
import { useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';
import { SubmitHandler, useForm } from 'react-hook-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';

import { Button, Input, Logo } from 'components/atoms';
import { LOGIN, REGISTER } from 'queries';
import { useAppDispatch } from 'hooks/useAppDispatch';
import { userSlice } from 'redux/reducers/UserSlice';

import PATHS from 'routes/PATHS';

import LogoSizeEnum from 'components/atoms/Logo/types/LogoSizeEnum';

import logo from 'assets/logo-light.svg';

import './Register.scss';

type RegisterInputsType = {
  name: string;
  username: string;
  email: string;
  password: string;
  repeatPassword: string;
};

const Register = (): ReactElement => {
  const { t } = useTranslation('RegisterPage');

  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [errorState, setErrorState] = useState(false);
  const [passwordErrorState, setPasswordErrorState] = useState(false);

  const dispatch = useAppDispatch();

  const { updateUser } = userSlice.actions;

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<RegisterInputsType>();

  const navigate = useNavigate();

  const onRegister: SubmitHandler<RegisterInputsType> = (data) => {
    void REGISTER(data.name, data.email, data.username, data.password).then((result) => {
      if (result.success) {
        void LOGIN(data.username, data.password).then((result) => {
          if (result.success && result.user) {
            dispatch(updateUser(result.user));
            navigate(`/${PATHS.MAIN}/${PATHS.HOME}`, { replace: true });
          } else {
            setErrorState(true);
          }
        });
      } else {
        setErrorState(true);
      }
    });
  };

  useEffect(() => {
    if (watch('password') !== watch('repeatPassword') && watch('repeatPassword')) {
      setPasswordErrorState(true);
    } else {
      setPasswordErrorState(false);
    }
  }, [watch('password'), watch('repeatPassword')]);

  useEffect(() => setErrorState(false), [watch('username'), watch('email')]);

  return (
    <section className={cn('section section-center register')}>
      <div className={'register-header'}>
        <Logo
          onClick={() => navigate(`${PATHS.MAIN}/${PATHS.HOME}`)}
          logo={logo}
          size={LogoSizeEnum.MEDIUM}
        />
      </div>
      <form onSubmit={handleSubmit(onRegister)} className={'register-form'}>
        <h3 className={'register-form-header'}>{t('headerText')}</h3>
        <label className={'register-label'}>
          {t('nameLabel')}
          <Input
            {...register('name', { required: true })}
            type="text"
            name="name"
            placeholder={t('namePlaceholder')}
            className={'register-input'}
          />
          {(errors.name || errorState) && (
            <div className={'error-box'}>
              <FontAwesomeIcon
                className={'error-icon'}
                icon={icon({ name: 'circle-exclamation' })}
              />
              <p className={'error-message'}>{t('nameError')}</p>
            </div>
          )}
        </label>
        <label className={'register-label'}>
          {t('usernameLabel')}
          <Input
            {...register('username', { required: true })}
            type="text"
            name="username"
            placeholder={t('uniqueUsernamePlaceholder')}
            className={'register-input'}
          />
          {(errors.username || errorState) && (
            <div className={'error-box'}>
              <FontAwesomeIcon
                className={'error-icon'}
                icon={icon({ name: 'circle-exclamation' })}
              />
              <p className={'error-message'}>{t('usernameExistsError')}</p>
            </div>
          )}
        </label>
        <label className={'register-label'}>
          {' '}
          {t('email')}
          <Input
            {...register('email', { required: true })}
            type="email"
            name="email"
            placeholder={t('validEmailPlaceholder')}
            className={'register-input'}
          />
          {(errors.username || errorState) && (
            <div className={'error-box'}>
              <FontAwesomeIcon
                className={'error-icon'}
                icon={icon({ name: 'circle-exclamation' })}
              />
              <p className={'error-message'}>{t('validEmailError')}</p>
            </div>
          )}
        </label>
        <label className={'register-label'}>
          {t('passwordLabel')}
          <div className={'register-input-box'}>
            <Input
              {...register('password', { required: true })}
              type={showPassword ? 'text' : 'password'}
              name="password"
              placeholder={t('passwordPlaceholder')}
              className={'register-input password'}
            />
            {showPassword ? (
              <FontAwesomeIcon
                onClick={() => setShowPassword(!showPassword)}
                className={'register-input-icon'}
                icon={icon({ name: 'eye-slash' })}
              />
            ) : (
              <FontAwesomeIcon
                onClick={() => setShowPassword(!showPassword)}
                className={'register-input-icon'}
                icon={icon({ name: 'eye' })}
              />
            )}
          </div>
          {(errors.password || passwordErrorState) && (
            <div className={'error-box'}>
              <FontAwesomeIcon
                className={'error-icon'}
                icon={icon({ name: 'circle-exclamation' })}
              />
              <p className={'error-message'}>{t('passwordMismatch')}</p>
            </div>
          )}
        </label>
        <label className={'register-label'}>
          {t('confirmPasswordLabel')}
          <div className={'register-input-box'}>
            <Input
              {...register('repeatPassword', { required: true })}
              type={showPassword ? 'text' : 'password'}
              name="repeatPassword"
              placeholder={t('repeatPasswordPlaceholder')}
              className={'register-input password'}
            />
            {showPassword ? (
              <FontAwesomeIcon
                onClick={() => setShowPassword(!showPassword)}
                className={'register-input-icon'}
                icon={icon({ name: 'eye-slash' })}
              />
            ) : (
              <FontAwesomeIcon
                onClick={() => setShowPassword(!showPassword)}
                className={'register-input-icon'}
                icon={icon({ name: 'eye' })}
              />
            )}
          </div>
          {(errors.password || passwordErrorState) && (
            <div className={'error-box'}>
              <FontAwesomeIcon
                className={'error-icon'}
                icon={icon({ name: 'circle-exclamation' })}
              />
              <p className={'error-message'}>{t('passwordMismatch')}</p>
            </div>
          )}
        </label>
        <Button type={'submit'} className={'register-btn'}>
          <span>{t('startJourneyBtn')}</span>
        </Button>
        <div className={'register-switch-box'}>
          <p className={'register-switch-text'}>{t('alreadyHaveAccount')} </p>
          <p className={'register-switch-link'} onClick={() => navigate(`/${PATHS.LOGIN}`)}>
            {t('signInHere')}
          </p>
        </div>
      </form>
    </section>
  );
};

export default Register;
