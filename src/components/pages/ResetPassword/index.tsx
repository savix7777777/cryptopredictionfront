import React, { ReactElement } from 'react';
import cn from 'classnames';
import { useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';
import { SubmitHandler, useForm } from 'react-hook-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';

import { Button, Input, Logo } from 'components/atoms';
import { RESET_PASSWORD } from 'queries';

import LogoSizeEnum from 'components/atoms/Logo/types/LogoSizeEnum';
import PATHS from 'routes/PATHS';

import logo from 'assets/logo-light.svg';

import './ResetPassword.scss';

type ResetPasswordInputsType = {
  email: string;
};

const ResetPassword = (): ReactElement => {
  const { t } = useTranslation('ResetPasswordPage');

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ResetPasswordInputsType>();

  const navigate = useNavigate();

  const onReset: SubmitHandler<ResetPasswordInputsType> = (data) => {
    void RESET_PASSWORD(data.email).then((response) => {
      console.log(response.success);
    });
  };

  return (
    <section className={cn('section section-center reset')}>
      <div className={'reset-header'}>
        <Logo
          onClick={() => navigate(`${PATHS.MAIN}/${PATHS.HOME}`)}
          logo={logo}
          size={LogoSizeEnum.MEDIUM}
        />
      </div>
      <form onSubmit={handleSubmit(onReset)} className={'reset-form'}>
        <h3 className={'reset-form-header'}>{t('resetPasswordHeader')}</h3>
        <p className={'reset-text-help'}>{t('enterEmailHelp')}</p>
        <label className={'reset-label'}>
          {t('emailLabel')}
          <Input
            {...register('email', { required: true })}
            type="email"
            name="email"
            placeholder={t('emailPlaceholder')}
            className={'reset-input'}
          />
          {errors.email && (
            <div className={'error-box'}>
              <FontAwesomeIcon
                className={'error-icon'}
                icon={icon({ name: 'circle-exclamation' })}
              />
              <p className={'error-message'}>{t('invalidEmailError')}</p>
            </div>
          )}
        </label>
        <Button disabled type={'submit'} className={'reset-btn'}>
          <span>{t('resetPasswordButton')}</span>
        </Button>
        <div className={'reset-switch-box'}>
          <p className={'reset-switch-text'}>{t('alreadyHaveAccount')} </p>
          <p className={'reset-switch-link'} onClick={() => navigate(`/${PATHS.LOGIN}`)}>
            {t('signInHere')}
          </p>
        </div>
      </form>
    </section>
  );
};

export default ResetPassword;
