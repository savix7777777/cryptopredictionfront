import React, { ReactElement, useCallback, useState } from 'react';
import cn from 'classnames';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';

import { Button, ErrorMessage, Loader } from 'components/atoms';
import {
  GET_ROADMAP,
  UPDATE_DISLIKES,
  UPDATE_LIKES,
  UPDATE_ROADMAP_DISLIKES,
  UPDATE_ROADMAP_LIKES,
} from 'queries';
import { modalSlice } from 'redux/reducers/ModalSlice';
import { userSlice } from 'redux/reducers/UserSlice';
import { useAppDispatch } from 'hooks/useAppDispatch';
import { useAppSelector } from 'hooks/useAppSelector';

import { RoadmapItemType } from 'queries/roadmap/GET_ROADMAP';

import './Roadmap.scss';

const Roadmap = (): ReactElement => {
  const { dark } = useAppSelector((state) => state.themeReducer);

  const {
    t,
    i18n: { language },
  } = useTranslation('RoadmapPage');

  const { data, isLoading, isError, error } = useQuery({
    queryKey: ['roadmap'],
    queryFn: () => GET_ROADMAP(),
  });

  const [loadingState, setLoadingState] = useState<boolean>(false);

  const { user } = useAppSelector((state) => state.userReducer);

  const queryClient = useQueryClient();
  const dispatch = useAppDispatch();

  const { updateModal } = modalSlice.actions;
  const { updateUser } = userSlice.actions;

  const handleLikeRoadmapItem = useCallback(
    async (item: RoadmapItemType) => {
      if (user.id) {
        if (!loadingState) {
          setLoadingState(true);
          await UPDATE_LIKES(user.id, item._id || '').then((response) => {
            if (response.rateConflict) {
              void UPDATE_ROADMAP_DISLIKES(item._id || '', -1).then(() => {});
            }
            dispatch(
              updateUser({
                ...user,
                likes: response.likes ?? [],
                dislikes: response.dislikes ?? [],
              }),
            );
          });
          if (user.likes.includes(item._id ?? '')) {
            await UPDATE_ROADMAP_LIKES(item._id || '', -1).then(() => {
              void queryClient.invalidateQueries(['roadmap']);
            });
          } else {
            await UPDATE_ROADMAP_LIKES(item._id || '', 1).then(() => {
              void queryClient.invalidateQueries(['roadmap']);
            });
          }
        }
      } else {
        dispatch(updateModal(true));
      }
      setLoadingState(false);
    },
    [user.id, user.likes, loadingState, dispatch],
  );

  const handleDislikeRoadmapItem = useCallback(
    async (item: RoadmapItemType) => {
      if (user.id) {
        if (!loadingState) {
          setLoadingState(true);
          await UPDATE_DISLIKES(user.id, item._id || '').then((response) => {
            if (response.rateConflict) {
              void UPDATE_ROADMAP_LIKES(item._id || '', -1).then(() => {});
            }
            dispatch(
              updateUser({
                ...user,
                likes: response.likes ?? [],
                dislikes: response.dislikes ?? [],
              }),
            );
          });
          if (user.dislikes.includes(item._id ?? '')) {
            await UPDATE_ROADMAP_DISLIKES(item._id || '', -1).then(() => {
              void queryClient.invalidateQueries(['roadmap']);
            });
          } else {
            await UPDATE_ROADMAP_DISLIKES(item._id || '', 1).then(() => {
              void queryClient.invalidateQueries(['roadmap']);
            });
          }
        }
      } else {
        dispatch(updateModal(true));
      }
      setLoadingState(false);
    },
    [user.id, user.dislikes, loadingState, dispatch],
  );

  return (
    <section className={cn('section roadmap')}>
      <div className={'roadmap-title-box'}>
        <h3 className={cn('roadmap-title', !dark ? 'light' : 'dark')}>{t('roadmapTitle')}</h3>
        <p className={cn('roadmap-title__text', !dark ? 'light' : 'dark')}>
          {t('roadmapSubtitle')} ✨
        </p>
      </div>
      <div className={'roadmap-list'}>
        {data &&
          'data' in data &&
          [...data.data[0].roadmap].reverse().map((item, index) => {
            const updates = item[`updates${language.toUpperCase()}`] as string[];
            return (
              <div key={index} className={'roadmap-list-item'}>
                <div className={'roadmap-chrono-box'}>
                  <div
                    className={cn(
                      'roadmap-chrono-line roadmap-chrono-line-start',
                      !dark ? 'light' : 'dark',
                    )}
                  />
                  <div className={cn('roadmap-chrono-dot', !dark ? 'light' : 'dark')} />
                  <div
                    className={cn(
                      'roadmap-chrono-line roadmap-chrono-line-end',
                      !dark ? 'light' : 'dark',
                    )}
                  />
                </div>
                <div className={'roadmap-content-box'}>
                  <h3 className={cn('roadmap-content-title', !dark ? 'light' : 'dark')}>
                    {item.release}
                  </h3>
                  <ul className={cn('roadmap-content-list', !dark ? 'light' : 'dark')}>
                    {updates.map((update, indexY) => (
                      <li key={indexY}>{update}</li>
                    ))}
                  </ul>
                  <div className={'roadmap-button-box'}>
                    <Button
                      className={cn(
                        'roadmap-button',
                        !dark ? 'light' : 'dark',
                        user.likes.includes(item._id ?? '') ? 'roadmap-button__rated' : '',
                      )}
                      onClick={() => handleLikeRoadmapItem(item)}
                    >
                      <FontAwesomeIcon
                        className={'roadmap-icon'}
                        icon={icon({ name: 'thumbs-up' })}
                      />
                      {item.likes}
                    </Button>
                    <Button
                      className={cn(
                        'roadmap-button',
                        !dark ? 'light' : 'dark',
                        user.dislikes.includes(item._id ?? '') ? 'roadmap-button__rated' : '',
                      )}
                      onClick={() => handleDislikeRoadmapItem(item)}
                    >
                      <FontAwesomeIcon
                        className={'roadmap-icon'}
                        icon={icon({ name: 'thumbs-down' })}
                      />
                      {item.dislikes}
                    </Button>
                  </div>
                </div>
              </div>
            );
          })}
        {data && (
          <div className={'roadmap-list-item'}>
            <div className={'roadmap-chrono-box'}>
              <div
                className={cn(
                  'roadmap-chrono-line roadmap-chrono-line-start',
                  !dark ? 'light' : 'dark',
                )}
              />
              <div className={cn('roadmap-chrono-dot', !dark ? 'light' : 'dark')} />
            </div>
            <div className={'roadmap-content-box'}>
              <h3 className={cn('roadmap-content-title', !dark ? 'light' : 'dark')}>
                {t('launching')} 🔥
              </h3>
            </div>
          </div>
        )}
        {isLoading && <Loader width={50} height={50} />}
        {isError && (
          <ErrorMessage
            errorHint={t('errorLoadingRoadmap')}
            errorText={error instanceof Error ? error.message : ''}
          />
        )}
      </div>
    </section>
  );
};

export default Roadmap;
