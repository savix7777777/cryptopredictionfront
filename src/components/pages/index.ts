export { default as Home } from './Home';
export { default as Crypto } from './Crypto';
export { default as CryptoDetail } from './CryptoDetail';
export { default as Roadmap } from './Roadmap';
export { default as Login } from './Login';
export { default as Register } from './Register';
export { default as ResetPassword } from './ResetPassword';
