import React, { Dispatch, ReactElement, SetStateAction } from 'react';
import cn from 'classnames';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';

import useWindowWidth from 'hooks/useWindowWidth';
import { Button, NavPanel } from 'components/atoms';
import { modalSlice } from 'redux/reducers/ModalSlice';
import { useAppDispatch } from 'hooks/useAppDispatch';
import { useAppSelector } from 'hooks/useAppSelector';
import { ControlPanel } from 'components/molecules';

import PATHS from 'routes/PATHS';

import './ControlPanelLayout.scss';

type ControlPanelLayoutPropType = {
  showPanel: boolean;
  setShowPanel: Dispatch<SetStateAction<boolean>>;
};

const ControlPanelLayout = ({
  showPanel,
  setShowPanel,
}: ControlPanelLayoutPropType): ReactElement => {
  const { t } = useTranslation('Header');

  const windowWidth = useWindowWidth();

  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const { dark } = useAppSelector((state) => state.themeReducer);
  const { updateModal } = modalSlice.actions;

  const { user } = useAppSelector((state) => state.userReducer);

  const navTabs = [
    { title: t('home'), link: PATHS.HOME },
    { title: t('crypto'), link: PATHS.CRYPTO },
    { title: t('roadmap'), link: PATHS.ROADMAP },
  ];

  const handleClickWatchList = () => {
    if (user.id) {
      navigate(`/${PATHS.MAIN}/${PATHS.CRYPTO}`, { state: 'watchlist' });
      setShowPanel(false);
    } else {
      dispatch(updateModal(true));
    }
  };

  return (
    <>
      {windowWidth <= 768 ? (
        <>
          <div
            style={{ left: showPanel ? 0 : '100vw' }}
            onClick={(event) => event.stopPropagation()}
            className={cn('control-panel-layout', !dark ? 'light' : 'dark')}
          >
            <div className={'control-panel__top-box'}>
              <NavPanel tabs={navTabs} onClick={() => setShowPanel(false)} />
              <Button
                onClick={handleClickWatchList}
                className={cn('watch-list', !dark ? 'light' : 'dark')}
              >
                <FontAwesomeIcon
                  className={cn('watch-list__star', !dark ? 'light' : 'dark')}
                  icon={icon({ name: 'star' })}
                />
                {t('watchlistButton')}
              </Button>
            </div>
            <ControlPanel />
          </div>
          {showPanel ? (
            <FontAwesomeIcon
              onClick={() => setShowPanel(false)}
              className={cn(
                'control-panel__close-btn',
                !dark ? (location.pathname.endsWith(PATHS.HOME) ? 'white' : 'light') : 'dark',
              )}
              icon={icon({ name: 'close' })}
            />
          ) : (
            <FontAwesomeIcon
              onClick={() => setShowPanel(true)}
              className={cn(
                'control-panel__close-btn',
                !dark ? (location.pathname.endsWith(PATHS.HOME) ? 'white' : 'light') : 'dark',
              )}
              icon={icon({ name: 'bars' })}
            />
          )}
        </>
      ) : (
        <ControlPanel />
      )}
    </>
  );
};

export default ControlPanelLayout;
