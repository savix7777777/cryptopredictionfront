import React, { ReactElement, useEffect, useState } from 'react';
import cn from 'classnames';
import { useLocation, useNavigate } from 'react-router';
import { useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';

import ControlPanelLayout from 'components/templates/ControlPanelLayout';
import useWindowWidth from 'hooks/useWindowWidth';
import { Button, Logo, NavPanel } from 'components/atoms';
import { SimpleStats } from 'components/molecules';
import { useAppSelector } from 'hooks/useAppSelector';
import { useAppDispatch } from 'hooks/useAppDispatch';
import { modalSlice } from 'redux/reducers/ModalSlice';

import LogoSizeEnum from 'components/atoms/Logo/types/LogoSizeEnum';

import PATHS from 'routes/PATHS';
import logoLight from 'assets/logo-light.svg';
import logoDark from 'assets/logo-dark.svg';

import './Header.scss';

const Header = (): ReactElement => {
  const { t } = useTranslation('Header');
  const location = useLocation();

  const [showPanel, setShowPanel] = useState(false);

  useEffect(() => {
    document.body.style.overflow = showPanel ? 'hidden' : 'scroll';
    if (showPanel) document.querySelector('.background')?.classList.add('no-scroll');
    else document.querySelector('.background')?.classList.remove('no-scroll');
  }, [showPanel]);

  const navTabs = [
    { title: t('home'), link: PATHS.HOME },
    { title: t('crypto'), link: PATHS.CRYPTO },
    { title: t('roadmap'), link: PATHS.ROADMAP },
  ];

  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const windowWidth = useWindowWidth();

  const { updateModal } = modalSlice.actions;

  const { user } = useAppSelector((state) => state.userReducer);
  const { dark } = useAppSelector((state) => state.themeReducer);

  const handleClickWatchList = () => {
    if (user.id) navigate(`/${PATHS.MAIN}/${PATHS.CRYPTO}`, { state: 'watchlist' });
    else {
      dispatch(updateModal(true));
    }
  };

  return (
    <header>
      <div className={'header-line first'}>
        {windowWidth > 768 ? (
          <SimpleStats />
        ) : (
          <Logo
            logo={
              !dark ? (location.pathname.endsWith(PATHS.HOME) ? logoLight : logoDark) : logoLight
            }
            size={LogoSizeEnum.MEDIUM}
          />
        )}
        <ControlPanelLayout showPanel={showPanel} setShowPanel={setShowPanel} />
      </div>
      <div className={cn('header-line second', !dark ? 'light' : 'dark')}>
        {windowWidth <= 768 ? (
          <SimpleStats />
        ) : (
          <>
            <Logo
              logo={
                !dark ? (location.pathname.endsWith(PATHS.HOME) ? logoLight : logoDark) : logoLight
              }
              icon={windowWidth >= 992}
              size={windowWidth <= 768 ? LogoSizeEnum.LARGE : LogoSizeEnum.MEDIUM}
            />
            <NavPanel tabs={navTabs} />
            <div className={'header-line-right'}>
              <Button
                onClick={handleClickWatchList}
                className={cn(
                  'watch-list',
                  location.pathname.endsWith(PATHS.HOME) && !dark ? 'white' : '',
                )}
              >
                <FontAwesomeIcon
                  className={cn('watch-list__star', !dark ? 'light' : 'dark')}
                  icon={icon({ name: 'star' })}
                />
                {t('watchlistButton')}
              </Button>
            </div>
          </>
        )}
      </div>
    </header>
  );
};

export default Header;
