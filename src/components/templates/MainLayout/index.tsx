import React, { ReactElement } from 'react';
import { Outlet } from 'react-router';

import Header from './Header';

const MainLayout = (): ReactElement => (
  <>
    <Header />
    <Outlet />
  </>
);

export default MainLayout;
