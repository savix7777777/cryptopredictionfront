import React, { ReactElement, ReactNode } from 'react';
import { Navigate } from 'react-router';

import PATHS from 'routes/PATHS';
import { MainLayout } from 'components/templates';

export type ProtectedRouteProps = {
  isAllowed: boolean;
  redirectPath?: string;
  children?: ReactNode;
  replace?: boolean;
};

const ProtectedRoute = ({
  isAllowed,
  redirectPath,
  children,
  replace,
}: ProtectedRouteProps): ReactElement => {
  if (!isAllowed) {
    return <Navigate to={redirectPath || PATHS.LOGIN} replace={replace || true} />;
  }

  return <>{children || <MainLayout />}</>;
};

export default ProtectedRoute;
