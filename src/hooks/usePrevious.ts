/* eslint-disable */

import { useEffect, useRef } from 'react';

const usePrevious = <T extends unknown>(value: T): T | undefined => {
  const currentValue = useRef<T>(value ? JSON.parse(JSON.stringify(value)) : value);
  const previousValue = useRef<T>();

  useEffect(() => {
    if (value && JSON.stringify(currentValue.current) !== JSON.stringify(value)) {
      previousValue.current =
        currentValue.current !== undefined
          ? JSON.parse(JSON.stringify(currentValue.current))
          : currentValue.current;
      currentValue.current = JSON.parse(JSON.stringify(value));
    } else if (!value && currentValue.current !== value) {
      previousValue.current = currentValue.current;
      currentValue.current = value;
    }
  }, [value]);

  return previousValue.current;
};

export default usePrevious;
