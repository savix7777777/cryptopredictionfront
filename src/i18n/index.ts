import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-xhr-backend';

void i18n
  .use(Backend)
  .use(initReactI18next)
  .init({
    lng: localStorage.getItem('language') || 'en',
    backend: {
      loadPath: '/locales/{{lng}}/{{ns}}.json',
    },
    fallbackLng: 'en',
    debug: false,
    ns: [
      'Comment',
      'Comments',
      'ControlPanel',
      'ControlPanelToggle',
      'CryptoDetailPage',
      'CryptoPage',
      'CryptoTitle',
      'Header',
      'HomePage',
      'ItemsList',
      'LoginPage',
      'Modal',
      'NotFound',
      'RegisterPage',
      'ResetPasswordPage',
      'RoadmapPage',
      'SearchList',
      'Stats',
      'TogglePlots',
    ],
    defaultNS: 'Home',
    keySeparator: false,
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
