import axios from 'axios';

import headers from '../headers.json';

type PromiseType = { success: boolean };

const DELETE_COMMENT = async (
  cryptocurrency: string,
  period: number,
  id: string,
): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');
    if (!cryptocurrency || !period || !id) throw Error('Wrong comment data');

    const url = `${serverUrl}/delete-comment/${cryptocurrency}/${period}/${id}`;

    const response = await axios.delete<unknown>(url, {
      headers: {
        ...headers,
      },
    });

    if (response) {
      return { success: true };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default DELETE_COMMENT;
