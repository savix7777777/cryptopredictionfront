import axios from 'axios';

import headers from '../headers.json';

type PromiseType = { data: unknown; success: boolean };

const GET_CRYPTO_CURRENT_PRICE = async (name: string): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/cryptos/${name}/current-price`;

    const response = await axios.get<{ currentPrice: string }>(url, {
      headers: {
        ...headers,
      },
    });

    if (response) {
      return { success: true, data: (+response.data.currentPrice).toFixed(2) };
    }

    return { success: false, data: null };
  } catch (error) {
    console.error(error);

    return { success: false, data: null };
  }
};

export default GET_CRYPTO_CURRENT_PRICE;
