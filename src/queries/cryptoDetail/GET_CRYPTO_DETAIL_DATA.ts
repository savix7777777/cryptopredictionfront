import axios from 'axios';

import { ICryptoDetailData } from '../globalData/GET_CRYPTOS_LIST_DATA';
import headers from '../headers.json';

type PromiseType = { data: ICryptoDetailData; success: boolean } | { success: boolean };

const GET_CRYPTO_DETAIL_DATA = async (name: string): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');
    if (!name) throw Error('Unknown cryptocurrency name');

    const url = `${serverUrl}/cryptos/${name}`;

    const response = await axios.get<ICryptoDetailData>(url, {
      headers: {
        ...headers,
      },
    });

    if (response) {
      return {
        success: true,
        data: response.data,
      };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default GET_CRYPTO_DETAIL_DATA;
