import axios from 'axios';

import headers from '../headers.json';

type PromiseType = { data: Array<number> | null; success: boolean };

interface ResponseType {
  [key: string]: [number];
}

const GET_CRYPTO_PRICE_PER_PERIOD = async (name: string, period: number): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/cryptos/${name}/histo-date/${period}`;

    const response = await axios.get<ResponseType>(url, {
      headers: {
        ...headers,
      },
    });

    if (response) {
      return { success: true, data: response.data[`histoDate${period}`] };
    }

    return { success: false, data: null };
  } catch (error) {
    console.error(error);

    return { success: false, data: null };
  }
};

export default GET_CRYPTO_PRICE_PER_PERIOD;
