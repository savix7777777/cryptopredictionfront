import axios from 'axios';

import headers from '../headers.json';

type PromiseType = { success: boolean };

export type CommentType = {
  author: string;
  authorUsername: string;
  text: string;
  date: string;
  authorId: string;
  period: number;
  name: string;
  likes: number;
  dislikes: number;
  replies: number;
  sentiment: string;
  repliedToAuthorUsername?: string | null;
  repliedToCommentId?: string | null;
  repliedToPeriod?: number | null;
  _id?: string;
};

const POST_COMMENT = async (cryptocurrency: string, comment: CommentType): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/add-comment`;

    const response = await axios.post<unknown>(
      url,
      {
        cryptocurrency,
        comment,
      },
      {
        headers: {
          ...headers,
        },
      },
    );

    if (response) {
      return { success: true };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default POST_COMMENT;
