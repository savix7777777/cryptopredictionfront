import axios from 'axios';

import headers from '../headers.json';

type PromiseType = { success: boolean };

const UPDATE_COMMENT = async (
  cryptocurrency: string,
  period: number,
  id: string,
  newText: string,
): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');
    if (!cryptocurrency || !period || !id || !newText) throw Error('Wrong comment data');

    const url = `${serverUrl}/update-comment/${cryptocurrency}/${period}/${id}`;

    const response = await axios.put<unknown>(url, {
      headers: {
        ...headers,
      },
      body: {
        newText,
      },
    });

    if (response) {
      return { success: true };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default UPDATE_COMMENT;
