import axios from 'axios';

import headers from '../headers.json';

type PromiseType = { success: boolean };

const UPDATE_COMMENT_DISLIKES = async (
  cryptocurrency: string,
  period: number,
  id: string,
  delta: number,
): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');
    if (!cryptocurrency || !period || !id || !delta) throw Error('Wrong comment data');

    const url = `${serverUrl}/update-comment-dislikes/${cryptocurrency}/${period}/${id}`;

    const response = await axios.put<unknown>(url, {
      headers: {
        ...headers,
      },
      body: {
        delta,
      },
    });

    if (response) {
      return { success: true };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default UPDATE_COMMENT_DISLIKES;
