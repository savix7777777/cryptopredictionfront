import axios from 'axios';

import headers from '../headers.json';

type PromiseType = { success: boolean };

const UPDATE_COMMENT_SENTIMENT = async (
  cryptocurrency: string,
  period: number,
  id: string,
  sentiment: string,
): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');
    if (!cryptocurrency || !period || !id) throw Error('Wrong comment data');

    const url = `${serverUrl}/update-comment-sentiment/${cryptocurrency}/${period}/${id}`;

    const response = await axios.put<unknown>(url, {
      headers: {
        ...headers,
      },
      body: {
        sentiment,
      },
    });

    if (response) {
      return { success: true };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default UPDATE_COMMENT_SENTIMENT;
