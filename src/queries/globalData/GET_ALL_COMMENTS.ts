import axios from 'axios';

import { CommentType } from '../cryptoDetail/POST_COMMENT';

import headers from '../headers.json';

export type AllCommentsType = {
  comments: Array<CommentType>;
};

type PromiseType = { data: AllCommentsType; success: boolean } | { success: boolean; data: null };

const GET_ALL_COMMENTS = async (): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/comments`;

    const response = await axios.get<AllCommentsType>(url, {
      headers: {
        ...headers,
      },
    });

    if (response) {
      return { success: true, data: response.data };
    }

    return { success: false, data: null };
  } catch (error) {
    console.error(error);

    return { success: false, data: null };
  }
};

export default GET_ALL_COMMENTS;
