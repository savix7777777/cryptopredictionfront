import axios from 'axios';

import headers from '../headers.json';

export type AllUsersAmountType = {
  usersAmount: number;
};

type PromiseType = { data: AllUsersAmountType; success: boolean } | { success: boolean };

const GET_ALL_USERS_AMOUNT = async (): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/usersAmount`;

    const response = await axios.get<AllUsersAmountType>(url, {
      headers: {
        ...headers,
      },
    });

    if (response) {
      return { success: true, data: response.data };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default GET_ALL_USERS_AMOUNT;
