import axios from 'axios';

import headers from '../headers.json';

export type AvailableCryptosType = {
  availableCryptos: Array<string> | number;
};

type PromiseType = { data: AvailableCryptosType; success: boolean } | { success: boolean };

const GET_AVAILABLE_CRYPTOS = async (returnCountInstead = false): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/cryptos`;

    const response = await axios.get<Array<string>>(url, {
      headers: {
        ...headers,
      },
    });

    if (response) {
      return {
        success: true,
        data: { availableCryptos: returnCountInstead ? response.data.length : response.data },
      };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default GET_AVAILABLE_CRYPTOS;
