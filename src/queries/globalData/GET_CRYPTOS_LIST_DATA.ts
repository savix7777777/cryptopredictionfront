import axios from 'axios';

import { CommentType } from '../cryptoDetail/POST_COMMENT';

import headers from '../headers.json';

export interface ICryptoDetailData {
  name: string;
  currentPrice: string;
  symbol: string;
  icon: string;
  website: string;
  reddit: string;
  data1D: {
    price: number;
    accuracy: number;
    predictionsHistory: Array<number>;
    comments: Array<CommentType>;
  };
  data7D: {
    price: number;
    accuracy: number;
    predictionsHistory: Array<number>;
    comments: Array<CommentType>;
  };
  data30D: {
    price: number;
    accuracy: number;
    predictionsHistory: Array<number>;
    comments: Array<CommentType>;
  };
}

type PromiseType = { data: Array<ICryptoDetailData>; success: boolean } | { success: boolean };

const GET_CRYPTOS_LIST_DATA = async (): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/cryptos-list`;

    const response = await axios.get<Array<ICryptoDetailData>>(url, {
      headers: {
        ...headers,
      },
    });

    if (response) {
      return {
        success: true,
        data: response.data,
      };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default GET_CRYPTOS_LIST_DATA;
