import axios from 'axios';

import headers from '../headers.json';

export type GlobalAccuracyType = {
  globalAccuracy: string;
};

type PromiseType = { data: GlobalAccuracyType; success: boolean } | { success: boolean };

const GET_MEAN_ACCURACY = async (): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/accuracy`;

    const response = await axios.get<GlobalAccuracyType>(url, {
      headers: {
        ...headers,
      },
    });

    if (response) {
      return { success: true, data: response.data };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default GET_MEAN_ACCURACY;
