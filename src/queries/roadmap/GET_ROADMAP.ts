import axios from 'axios';

import headers from '../headers.json';

export type RoadmapItemType = {
  dislikes: number;
  likes: number;
  release: string;
  [key: string]: string[] | number | string;
  _id: string;
};

export type RoadmapType = Array<{
  _id: string;
  roadmap: Array<RoadmapItemType>;
}>;

type PromiseType = { data: RoadmapType; success: boolean } | { success: boolean };

const GET_ROADMAP = async (): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/roadmap`;

    const response = await axios.get<RoadmapType>(url, {
      headers: {
        ...headers,
      },
    });

    if (response) {
      return { success: true, data: response.data };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default GET_ROADMAP;
