import axios from 'axios';

import headers from '../headers.json';

type PromiseType = { success: boolean };

const UPDATE_ROADMAP_LIKES = async (roadmapItemId: string, delta: number): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');
    if (!roadmapItemId || !delta) throw Error('Wrong comment data');

    const url = `${serverUrl}/update-roadmap-likes/${roadmapItemId}`;

    const response = await axios.put<unknown>(url, {
      headers: {
        ...headers,
      },
      body: {
        delta,
      },
    });

    if (response) {
      return { success: true };
    }

    return { success: false };
  } catch (error) {
    console.error(error);

    return { success: false };
  }
};

export default UPDATE_ROADMAP_LIKES;
