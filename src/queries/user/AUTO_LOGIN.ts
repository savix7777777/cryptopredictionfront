import axios from 'axios';

import headers from '../headers.json';

import { UserType } from './REGISTER';

type PromiseType = {
  success: boolean;
  user: UserType | null;
};

const AUTO_LOGIN = async (): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/auto-login`;

    const response = await axios.get<{
      message: string;
      user: UserType;
    }>(url, {
      headers: {
        ...headers,
      },
      withCredentials: true,
    });

    if (response.status === 200) {
      return {
        success: true,
        user: response.data.user,
      };
    }

    return { success: false, user: null };
  } catch (error) {
    console.error(error);
    return { success: false, user: null };
  }
};

export default AUTO_LOGIN;
