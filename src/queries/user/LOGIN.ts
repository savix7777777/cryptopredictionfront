import axios from 'axios';

import { UserType } from './REGISTER';

import headers from '../headers.json';

type PromiseType = {
  success: boolean;
  user: UserType | null;
};

const LOGIN = async (username: string, password: string): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/login`;

    const response = await axios.post<{
      message: string;
      user: UserType;
    }>(
      url,
      {
        username,
        password,
      },
      {
        headers: {
          ...headers,
        },
        withCredentials: true,
      },
    );

    if (response.status === 200) {
      return { success: true, user: response.data.user };
    }

    return { success: false, user: null };
  } catch (error) {
    console.error(error);
    return { success: false, user: null };
  }
};

export default LOGIN;
