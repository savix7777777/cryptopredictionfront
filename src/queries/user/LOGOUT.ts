import axios from 'axios';

import headers from '../headers.json';

type PromiseType = {
  success: boolean;
};

const LOGOUT = async (): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/logout`;

    const response = await axios.get(url, {
      headers: {
        ...headers,
      },
      withCredentials: true,
    });

    if (response.status === 200) {
      return { success: true };
    }

    return { success: false };
  } catch (error) {
    console.error(error);
    return { success: false };
  }
};

export default LOGOUT;
