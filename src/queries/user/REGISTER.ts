import axios from 'axios';

import headers from '../headers.json';

type PromiseType = {
  success: boolean;
};

export type UserType = {
  name: string;
  username: string;
  id: string;
  watchlist: string[];
  subscribes: string[];
  likes: string[];
  dislikes: string[];
};

const REGISTER = async (
  name: string,
  email: string,
  username: string,
  password: string,
): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/register`;

    const response = await axios.post<{
      message: string;
    }>(
      url,
      {
        name,
        email,
        username,
        password,
      },
      {
        headers: {
          ...headers,
        },
      },
    );

    if (response.status === 200) {
      return { success: true };
    }

    return { success: false };
  } catch (error) {
    console.error(error);
    return { success: false };
  }
};

export default REGISTER;
