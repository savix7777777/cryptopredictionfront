import axios from 'axios';

import headers from '../headers.json';

type PromiseType = {
  success: boolean;
};

const RESET_PASSWORD = async (email: string): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/reset-password`;

    const response = await axios.post<{
      message: string;
    }>(
      url,
      {
        email,
      },
      {
        headers: {
          ...headers,
        },
        withCredentials: true,
      },
    );

    if (response.status === 200) {
      return { success: true };
    }

    return { success: false };
  } catch (error) {
    console.error(error);
    return { success: false };
  }
};

export default RESET_PASSWORD;
