import axios from 'axios';

import headers from '../headers.json';

type PromiseType = {
  success: boolean;
  likes: string[] | null;
  dislikes: string[] | null;
  rateConflict: boolean;
};

const UPDATE_LIKES = async (userId: string, likeObjId: string): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/likes`;

    const response = await axios.post<{
      message: string;
      likes: string[] | null;
      dislikes: string[] | null;
      rateConflict: boolean;
    }>(
      url,
      {
        userId,
        likeObjId,
      },
      {
        headers: {
          ...headers,
        },
      },
    );

    if (response.status === 200) {
      return {
        success: true,
        likes: response.data.likes,
        dislikes: response.data.dislikes,
        rateConflict: response.data.rateConflict,
      };
    }

    return { success: false, likes: null, dislikes: null, rateConflict: false };
  } catch (error) {
    console.error(error);
    return { success: false, likes: null, dislikes: null, rateConflict: false };
  }
};

export default UPDATE_LIKES;
