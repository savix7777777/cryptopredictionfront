import axios from 'axios';

import headers from '../headers.json';

type PromiseType = {
  success: boolean;
  subscribes: string[] | null;
};

const UPDATE_SUBSCRIBES = async (userId: string, crypto: string): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/subscribes`;

    const response = await axios.post<{
      message: string;
      subscribes: string[] | null;
    }>(
      url,
      {
        userId,
        crypto,
      },
      {
        headers: {
          ...headers,
        },
      },
    );

    if (response.status === 200) {
      return { success: true, subscribes: response.data.subscribes };
    }

    return { success: false, subscribes: null };
  } catch (error) {
    console.error(error);
    return { success: false, subscribes: null };
  }
};

export default UPDATE_SUBSCRIBES;
