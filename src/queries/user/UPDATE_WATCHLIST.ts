import axios from 'axios';

import headers from '../headers.json';

type PromiseType = {
  success: boolean;
  watchlist: string[] | null;
};

const UPDATE_WATCHLIST = async (userId: string, crypto: string): Promise<PromiseType> => {
  try {
    const serverUrl = process.env.REACT_APP_SERVER_URL_BACKEND;
    if (!serverUrl) throw Error('Backend-URL not defined.');

    const url = `${serverUrl}/watchlist`;

    const response = await axios.post<{
      message: string;
      watchlist: string[] | null;
    }>(
      url,
      {
        userId,
        crypto,
      },
      {
        headers: {
          ...headers,
        },
      },
    );

    if (response.status === 200) {
      return { success: true, watchlist: response.data.watchlist };
    }

    return { success: false, watchlist: null };
  } catch (error) {
    console.error(error);
    return { success: false, watchlist: null };
  }
};

export default UPDATE_WATCHLIST;
