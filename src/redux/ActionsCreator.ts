import { createAsyncThunk } from '@reduxjs/toolkit';

import { AUTO_LOGIN } from 'queries';

import { UserType } from 'queries/user/REGISTER';

export const autoLogin = createAsyncThunk<UserType, void, { rejectValue: string }>(
  'user/autoLogin',
  async (_, thunkAPI) => {
    try {
      const response = await AUTO_LOGIN();
      if (!response.user) {
        return thunkAPI.rejectWithValue('Authorization failed');
      }
      return response.user;
    } catch (e) {
      return thunkAPI.rejectWithValue('Authorization failed');
    }
  },
);
