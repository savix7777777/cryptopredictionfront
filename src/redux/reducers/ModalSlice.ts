import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface ModalState {
  modal: boolean;
}

type ModalReducers = {
  updateModal: (state: ModalState, action: PayloadAction<boolean>) => void;
};

const initialState: ModalState = {
  modal: false,
};

export const modalSlice = createSlice<ModalState, ModalReducers>({
  name: 'modal',
  initialState,
  reducers: {
    updateModal(state, action: PayloadAction<boolean>) {
      state.modal = action.payload;
    },
  },
});

export default modalSlice.reducer;
