import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface ThemeState {
  dark: boolean;
}

type ThemeReducers = {
  updateTheme: (state: ThemeState, action: PayloadAction<boolean>) => void;
};

const initialState: ThemeState = {
  dark: localStorage.getItem('theme') == null ? true : localStorage.getItem('theme') === 'true',
};

export const themeSlice = createSlice<ThemeState, ThemeReducers>({
  name: 'theme',
  initialState,
  reducers: {
    updateTheme(state, action: PayloadAction<boolean>) {
      state.dark = action.payload;
      localStorage.setItem('theme', String(action.payload));
    },
  },
});

export default themeSlice.reducer;
