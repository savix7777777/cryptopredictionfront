import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { autoLogin } from '../ActionsCreator';

import { UserType } from 'queries/user/REGISTER';

interface UserState {
  user: UserType;
  isLoading: boolean;
  error: string;
}

type UserReducers = {
  updateUser: (state: UserState, action: PayloadAction<UserType>) => void;
  clearUser: (state: UserState) => void;
};

const initialState: UserState = {
  user: {
    name: 'Unknown',
    username: 'unknown',
    id: '',
    watchlist: [],
    subscribes: [],
    likes: [],
    dislikes: [],
  },
  isLoading: false,
  error: '',
};

export const userSlice = createSlice<UserState, UserReducers>({
  name: 'user',
  initialState,
  reducers: {
    updateUser(state, action: PayloadAction<UserType>) {
      state.user = action.payload;
    },
    clearUser(state) {
      state.user = {
        name: 'Unknown',
        username: 'unknown',
        id: '',
        watchlist: [],
        subscribes: [],
        likes: [],
        dislikes: [],
      };
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(autoLogin.fulfilled, (state, action: PayloadAction<UserType>) => {
        state.isLoading = false;
        state.error = '';
        state.user = action.payload;
      })
      .addCase(autoLogin.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(autoLogin.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload ?? action.error.message ?? '';
      });
  },
});

export default userSlice.reducer;
