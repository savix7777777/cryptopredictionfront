import { combineReducers, configureStore, Store } from '@reduxjs/toolkit';

import userReducer from './reducers/UserSlice';
import modalReducer from './reducers/ModalSlice';
import themeReducer from './reducers/ThemeSlice';

const rootReducer = combineReducers({
  userReducer,
  modalReducer,
  themeReducer,
});

export const setupStore = (): Store<ReturnType<typeof rootReducer>> =>
  configureStore({
    reducer: rootReducer,
  });

export type RootStateType = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore['dispatch'];
