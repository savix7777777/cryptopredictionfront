enum PATHS {
  ROOT = '/',
  MAIN = 'coinprophet',
  HOME = 'home',
  CRYPTO = 'crypto',
  ROADMAP = 'roadmap',
  LOGIN = 'login',
  REGISTER = 'register',
  RESET_PASSWORD = 'reset-password',
}

export default PATHS;
