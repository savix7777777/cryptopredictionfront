export const getDaysDataInterval = (interval: number, future: boolean): Array<string> => {
  const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  const today = new Date();

  const lastSevenDays = [];

  const condition = future ? 6 : 7;

  for (let i = future ? -1 : 0; i < condition; i++) {
    const date = new Date(today);
    date.setDate(today.getDate() - i * interval);
    const formattedDate = `${date.getDate()} ${months[date.getMonth()]}`;
    lastSevenDays.push(formattedDate);
  }

  return lastSevenDays;
};
