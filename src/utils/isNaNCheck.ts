import { JSX } from 'react';

export const isNaNCheck = (value: string | number | JSX.Element): boolean =>
  isNaN(+String(value).replaceAll('%', '').replaceAll('-', '0'));
export const preventNaN = (value: string | number | JSX.Element): number =>
  +String(value).replaceAll('%', '').replaceAll('-', '0');
