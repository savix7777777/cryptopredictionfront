const meanAccuracy = (accuraciesArr: Array<number>): number => {
  let validateLength = accuraciesArr.length;
  return (
    accuraciesArr.reduce((accum, number) => {
      if (number) {
        return accum + number;
      } else {
        validateLength -= 1;
        return accum;
      }
    }, 0) / validateLength
  );
};

export default meanAccuracy;
