const srollToTop = (element: Element, duration = 500): void => {
  const startingY = element.scrollTop;
  const difference = -startingY;
  let start: number | undefined;

  const step = (timestamp: number): void => {
    if (!start) start = timestamp;

    const time = timestamp - start;
    const percent = Math.min(time / duration, 1);
    element.scrollTop = startingY + difference * percent;

    if (time < duration) requestAnimationFrame(step);
  };

  requestAnimationFrame(step);
};

export default srollToTop;
