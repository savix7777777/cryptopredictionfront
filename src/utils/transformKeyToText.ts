import capitalize from './capitalize';

const transformKeyToText = (str: string): string | null => {
  let find = false;
  for (let i = 0; i < str.length; i++) {
    if (str[i] === str[i].toUpperCase() && !find) {
      find = true;
      const strArr = str.split(str[i]);
      strArr[1] = str[i] + strArr[1];
      return capitalize(strArr[0] + ' ' + strArr[1]);
    }
  }
  return null;
};

export default transformKeyToText;
