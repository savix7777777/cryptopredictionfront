const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// TEST1
require('dotenv').config();

module.exports = (env, options) => {
  const dev = process.env.NODE_ENV !== 'production';

  return {
    devtool: dev ? 'source-map' : false,
    target: 'browserslist',
    entry: {
      main: [
        './src/index.tsx'
      ],
    },
    output: {
      publicPath: '/',
      devtoolModuleFilenameTemplate: 'webpack:///[resource-path]?[loaders]',
      clean: true,
    },
    resolve: {
      extensions: ['*', '.ts', '.tsx', '.html', '.js', '.jsx'],
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx|tsx|ts)$/,
          include: [path.resolve('src')],
          loader: 'babel-loader'
        },
        {
          test: /\.html$/,
          use: 'html-loader',
        },
        {
          test: /\.css$/,
          use: [
            dev ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
          ],
        },
        {
          test: /\.s[ac]ss$/,
          use: [
            dev ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.(png|jpg|jpeg|gif|ico)$/,
          exclude: /node_modules/,
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
          },
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: './public/index.html',
        chunks: ['main'],
      }),
      new webpack.ProvidePlugin({
        Promise: ['es6-promise', 'Promise'],
      }),
      new webpack.DefinePlugin({
        'process.env': JSON.stringify(process.env)
      }),
      !dev && new MiniCssExtractPlugin(),
    ].filter(Boolean),
    devServer: {
      hot: true,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      port: process.env.NPM_PACKAGE_CONFIG_DEV_SERVER_PORT || 3000,
    },
  };
};
